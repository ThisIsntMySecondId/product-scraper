// ? Microless List Model

const mongoose = require('mongoose');
const Microless = {
    schema: {
        _id: String,
        title: String,
        link: String,
        sell_price: String,
        cost_price: String,
        image: String,
    },
    modelname: 'microless',
}

const MicrolessSchema = new mongoose.Schema(Microless.schema, { timestamps: true });
const MicrolessModel = mongoose.model(Microless.modelname, MicrolessSchema);

module.exports = MicrolessModel;