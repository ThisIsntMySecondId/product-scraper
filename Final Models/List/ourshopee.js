// ? Ourshopee List Model

const mongoose = require('mongoose');
const Ourshopee = {
    schema: {
        _id: String,
        title: String,
        link: String,
        cost_price: String,
        sell_price: String,
        image: String,
    },
    modelname: 'ourshopee',
}

const OurshopeeSchema = new mongoose.Schema(Ourshopee.schema, { timestamps: true });
const OurshopeeModel = mongoose.model(Ourshopee.modelname, OurshopeeSchema);

module.exports = OurshopeeModel;