// ? Ubuy List Model

const mongoose = require('mongoose');
const Ubuy = {
    schema: {
        _id: String,
        title: String,
        sell_price: String,
        cost_price: String,
        link: String,
        image: String,
    },
    modelname: 'ubuy',
}

const UbuySchema = new mongoose.Schema(Ubuy.schema, { timestamps: true });
const UbuyModel = mongoose.model(Ubuy.modelname, UbuySchema);

module.exports = UbuyModel;