// ? Sharafdg List Model

const mongoose = require('mongoose');
const Sharafdg = {
    schema: {
        _id: String,
        title: String,
        cost_price: String,
        sell_price: String,
        ratings: String,
        discount: String,
        link: String,
        image: String,
    },
    modelname: 'sharafdg',
}

const SharafdgSchema = new mongoose.Schema(Sharafdg.schema, { timestamps: true });
const SharafdgModel = mongoose.model(Sharafdg.modelname, SharafdgSchema);

module.exports = SharafdgModel;