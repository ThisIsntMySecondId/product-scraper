// ? Shopkees List Model

const mongoose = require('mongoose');
const Shopkees = {
    schema: {
        _id: String,
        title: String,
        link: String,
        image: String,
        cost_price: String,
        sell_price: String,
    },
    modelname: 'shopkees',
}

const ShopkeesSchema = new mongoose.Schema(Shopkees.schema, { timestamps: true });
const ShopkeesModel = mongoose.model(Shopkees.modelname, ShopkeesSchema);

module.exports = ShopkeesModel;