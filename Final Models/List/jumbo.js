// ? Jumbo List Model

const mongoose = require('mongoose');
const Jumbo = {
    schema: {
        _id: String,
        title: String,
        link: String,
        image: String,
        cost_price: String,
        sell_price: String,
        ratings: String,
        reviews: String,
    },
    modelname: 'jumbo',
}

const JumboSchema = new mongoose.Schema(Jumbo.schema, { timestamps: true });
const JumboModel = mongoose.model(Jumbo.modelname, JumboSchema);

module.exports = JumboModel;