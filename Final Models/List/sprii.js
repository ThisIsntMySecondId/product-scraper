// ? Sprii List Model

const mongoose = require('mongoose');
const Sprii = {
    schema: {
        _id: String,
        title: String,
        link: String,
        sell_price: String,
        cost_price: String,
        image: String,
    },
    modelname: 'sprii',
}

const SpriiSchema = new mongoose.Schema(Sprii.schema, { timestamps: true });
const SpriiModel = mongoose.model(Sprii.modelname, SpriiSchema);

module.exports = SpriiModel;