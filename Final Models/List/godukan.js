// ? Godukan List Model

const mongoose = require('mongoose');
const Godukan = {
    schema: {
        _id: String,
        title: String,
        link: String,
        image: String,
        cost_price: String,
        sell_price: String,
    },
    modelname: 'godukan',
}

const GodukanSchema = new mongoose.Schema(Godukan.schema, { timestamps: true });
const GodukanModel = mongoose.model(Godukan.modelname, GodukanSchema);

module.exports = GodukanModel;