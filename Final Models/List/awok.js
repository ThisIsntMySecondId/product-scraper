// ? Awok List Model

const mongoose = require('mongoose');
const Awok = {
    schema: {
        _id: String,
        title: String,
        link: String,
        cost_price: String,
        sell_price: String,
        discount: String,
        image: String,
    },
    modelname: 'awok',
}

const AwokSchema = new mongoose.Schema(Awok.schema, { timestamps: true });
const AwokModel = mongoose.model(Awok.modelname, AwokSchema);

module.exports = AwokModel;