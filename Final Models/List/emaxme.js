// ? Emaxme List Model

const mongoose = require('mongoose');
const Emaxme = {
    schema: {
        _id: String,
        title: String,
        link: String,
        image: String,
        cost_price: String,
        sell_price: String,
        discount: String,
    },
    modelname: 'emaxme',
}

// TOFIX: generate mongoose model here
const EmaxmeSchema = new mongoose.Schema(Emaxme.schema, { timestamps: true });
const EmaxmeModel = mongoose.model(Emaxme.modelname, EmaxmeSchema);

module.exports = EmaxmeModel;