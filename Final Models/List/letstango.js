// ? LetsTango List Model

const mongoose = require('mongoose');
const LetsTango = {
    schema: {
        _id: String,
        title: String,
        link: String,
        sell_price: String,
        cost_price: String,
        image: String,
    },
    modelname: 'letstango',
}

const LetsTangoSchema = new mongoose.Schema(LetsTango.schema, { timestamps: true });
const LetsTangoModel = mongoose.model(LetsTango.modelname, LetsTangoSchema);

module.exports = LetsTangoModel;