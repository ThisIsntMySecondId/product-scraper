// ? _ModelName_ Info Model

const mongoose = require('mongoose');

const Emaxme = {
    schema: {
        _id: String,
        modelNo: String,
        title: String,
        merchant: String,
        mrp: String,
        sellingPrice: String,
        // offers: [String], //not technically offers
        // ratingAndReviews: { rating: String, reviews: String },
        // category: [String],
        main_image: String,
        image_gallery: [String],
        key_features: [String],
        description: String,
        specifications: [{
            key: String,
            value: String
        }],
        // color: String, //TOFIX: not all colors are available in this selector
        // hardware_spec: String,
        // variation: [{
        // product_id: String,
        // name: String,
        // link: String,
        // }],
    },
    modelname: 'emaxmeinfo',

}

const EmaxmeSchema = new mongoose.Schema(Emaxme.schema, { timestamps: true });
const EmaxmeModel = mongoose.model(Emaxme.modelname, EmaxmeSchema);

module.exports = EmaxmeModel;