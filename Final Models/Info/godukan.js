// ? Godukan Info Model

const mongoose = require('mongoose');

const Godukan = {
    schema: {
        _id: String,
        modelNo: String,
        brand: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        // offers: [String],
        // ratingAndReviews: { rating: String, reviews: String },
        category: [String],
        main_image: String,
        image_gallery: [String],
        // key_features: [{
        //     key: String,
        //     value:  String,
        //     desc:  String
        // }],
        description: String,
        specifications: [{
                key: String,
                value: String
        }],
        // color: String,
        // hardware_spec: String,
        // variation: [{
        //     product_id: String,
        //     name: String,
        //     link: String,
        // }],
    },
    modelname: 'godukaninfo',

}


const GodukanSchema = new mongoose.Schema(Godukan.schema, { timestamps: true });
const GodukanModel = mongoose.model(Godukan.modelname, GodukanSchema);

module.exports = GodukanModel;