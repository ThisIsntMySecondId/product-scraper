// ? Awok Info Model

const mongoose = require('mongoose');

const Awok = {
    schema: {
        _id: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        // offers: [String], //not technically offers
        // ratingAndReviews: { rating: String, reviews: String },
        category: [String],
        main_image: String,
        image_gallery: [String],
        // key_features: [{
        //     key: String,
        //     value:  String,
        //     desc:  String
        // }],
        description: String,
        specifications: [{
                key: String,
                value: String
        }],
        color: String,
        hardware_spec: String,
        variation: {
            storage: [{
                size: String,
                link: String,
            }],
            colors: [{
                size: String,
                link: String,
            }],
        },
    },
    modelname: 'awokinfo',
}

const AwokSchema = new mongoose.Schema(Awok.schema, { timestamps: true });
const AwokModel = mongoose.model(Awok.modelname, AwokSchema);

module.exports = AwokModel;