// ? returns individual product
// ? tailored specifically for puppeteer scrapping

// ? _ModelName_ Info Model

const mongoose = require('mongoose');

const Sharafdg = {
    schema: {
        _id: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        offers: [String], //not technically offers
        ratingAndReviews: { rating: String, reviews: String },
        category: [String],
        main_image: String,
        image_gallery: [String],
        key_features: [{
            key: String,
            value:  String,
            desc:  String
        }],
        description: String,
        specifications: [{
            groupTitle: String,
            specs: [{
                key: String,
                value: String
            }]
        }],
        color: String,
        hardware_spec: String,
        variation: [{
            product_id: String,
            name: String,
            link: String,
        }],
    },
    modelname: 'sharafdgproductinfo',
}

const SharafdgSchema = new mongoose.Schema(Sharafdg.schema, { timestamps: true });
const SharafdgModel = mongoose.model(Sharafdg.modelname, SharafdgSchema);

module.exports = SharafdgModel;