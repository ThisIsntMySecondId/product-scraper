// ? _ModelName_ Info Model

const mongoose = require('mongoose');

const Shopkees = {
    schema: {
        _id: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        // offers: [String], //not technically offers
        ratingAndReviews: { rating: String, reviews: String },
        category: [String],
        main_image: String,
        image_gallery: [String],
        key_features: String,
        description: String,
        specifications: [{
                key: String,
                value: String
        }],
        // color: String, //TOFIX: not all colors are available in this selector
        // hardware_spec: String,
        // variation: [{
        //     product_id: String,
        //     name: String,
        //     link: String,
        // }],
    },
    modelname: 'shopkeesinfo',

}

const ShopkeesSchema = new mongoose.Schema(Shopkees.schema, { timestamps: true });
const ShopkeesModel = mongoose.model(Shopkees.modelname, ShopkeesSchema);

module.exports = ShopkeesModel;