// ? returns individual product
// ? tailored specifically for puppeteer scrapping

// ? _ModelName_ Info Model

const mongoose = require('mongoose');

const Jumbo = {
    schema: {
        // ? 💡 if the product have id then it will be used to generate id or 
       //? 👇 else products link will be used to generate id or else the title will be used to generate id
        _id: String,
        merchant: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        category: [String],
        main_image: String,
        image_gallery: [String],
        description: String,
        color: String,
        hardware_spec: String,
        ratingAndReviews: { rating: String, reviews: String },
        specifications: [{
                key: String,
                value: String
        }],
        variation: {
            colors: [String],
            storage: [String],
        },
    },
    modelname: 'jumboinfo',
}

const JumboSchema = new mongoose.Schema(Jumbo.schema, { timestamps: true });
const JumboModel = mongoose.model(Jumbo.modelname, JumboSchema);

module.exports = JumboModel;