// ? Sprii Info Model

const mongoose = require('mongoose');

const Sprii = {
    schema: {
        _id: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        category: [String],
        main_image: String,
        image_gallery: [String],
        key_features: [{
            key: String,
            value:  String,
            desc:  String
        }],
        description: String,
    },
    modelname: 'spriiProductinfo', // ? 📝 Model name is same as collection name with all letters lowercased and an 's' appended to the end
}

// TOFIX: generate mongoose model here
const SpriiSchema = new mongoose.Schema(Sprii.schema, { timestamps: true });
const SpriiModel = mongoose.model(Sprii.modelname, SpriiSchema);

module.exports = SpriiModel;