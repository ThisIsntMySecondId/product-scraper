// ? Ubuy Info Model

const mongoose = require('mongoose');

const Ubuy = {
    schema: {
        _id: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        // offers: [String], //not technically offers
        // ratingAndReviews: { rating: String, reviews: String },
        category: [String],
        main_image: String,
        image_gallery: [String],
        key_features: [String],
        description: String,
        specifications: [{
                key: String,
                value: String
        }],
        // color: String, //TOFIX: not all colors are available in this selector
        // hardware_spec: String,
        variation: [{
            variationParam: String,
            variationValues: [{
                name: String,
                asin: String
            }]
        }],
    },
    modelname: 'ubuyinfo',
}

const UbuySchema = new mongoose.Schema(Ubuy.schema, { timestamps: true });
const UbuyModel = mongoose.model(Ubuy.modelname, UbuySchema);

module.exports = UbuyModel;