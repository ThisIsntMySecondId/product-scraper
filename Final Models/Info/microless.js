// ? Microless Info Model

const mongoose = require('mongoose');

const Microless = {
    schema: {
        _id: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        // offers: [String], //not technically offers
        // ratingAndReviews: { rating: String, reviews: String },
        category: [String],
        main_image: String,
        image_gallery: [String],
        // key_features: [{
        //     key: String,
        //     value:  String,
        //     desc:  String
        // }],
        description: String,
        specifications: [{
            key: String,
            value: String
        }],
        // color: String, //TOFIX: not all colors are available in this selector
        // hardware_spec: String,
        // variation: [{
        //     product_id: String,
        //     name: String,
        //     link: String,
        // }],
    },
    modelname: 'microlessinfo',
}

const MicrolessSchema = new mongoose.Schema(Microless.schema, { timestamps: true });
const MicrolessModel = mongoose.model(Microless.modelname, MicrolessSchema);

module.exports = MicrolessModel;