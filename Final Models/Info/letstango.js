// ? Letstango Info Model

const mongoose = require('mongoose');

const Letstango = {
    schema: {
        _id: String,        //FIXME: no id nor link available for unique id 
        title: String,
        mrp: String,
        sellingPrice: String,
        // offers: [String],
        // ratingAndReviews: { rating: String, reviews: String },
        category: [String],
        main_image: String,
        image_gallery: [String],
        key_features: [String],
        description: String,
        specifications: [{
            specs: [{
                key: String,
                value: String
            }]
        }],
        color: String,
        hardware_spec: String,
        variation: [{
            variationParam: String,
            variations: [{
                value: String,
                link: String
            }]
        }],
    },
    modelname: 'letstangoproductinfo',
}

const LetstangoSchema = new mongoose.Schema(Letstango.schema, { timestamps: true });
const LetstangoModel = mongoose.model(Letstango.modelname, LetstangoSchema);

module.exports = LetstangoModel;