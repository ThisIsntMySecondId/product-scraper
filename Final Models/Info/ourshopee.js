// ? Ourshopee Info Model

const mongoose = require('mongoose');

const Ourshopee = {
    schema: {
        _id: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        // offers: [String],
        ratingAndReviews: { rating: String, reviews: String },
        category: [String],
        main_image: String,
        image_gallery: [String],
        key_features: [{
            key: String,
            value:  String,
        }],
        description: String,
        specifications: [{
                key: String,
                value: String
        }],
        color: String,
        hardware_spec: String,
        variation: {
            color: [{
                name: String,
                link: String,
            }],
            storage: [{
                name: String,
                link: String
            }]
        },
    },
    modelname: 'ourshopeeinfo',
}

const OurshopeeSchema = new mongoose.Schema(Ourshopee.schema, { timestamps: true });
const OurshopeeModel = mongoose.model(Ourshopee.modelname, OurshopeeSchema);

module.exports = OurshopeeModel;