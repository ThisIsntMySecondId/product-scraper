// ? returns individual product
// ? tailored specifically for puppeteer scrapping

// ? _MerchantName_ List Model

const mongoose = require('mongoose');
// FIXME: fix for products that doesnt have ids
const _MerchantName_ = {
    schema: {
        _id: String,
        title: String,
        link: String,
        // ? 👆 this will be used for id generation by default if the data retrievd have no id field FIXME 💥 not yet to be tested totally
        // ? if retrieved data doesnt have both id or link, results to error
        price: String,
        image: String,
        // ...
    },
    modelname: '', // ? collection where the data needs to be stored
}

// TOFIX: generate mongoose model here
const _MerchantName_Schema = new mongoose.Schema(_MerchantName_.schema, { timestamps: true });
const _MerchantName_Model = mongoose.model(_MerchantName_.modelname, _MerchantName_Schema);

module.exports = _MerchantName_Model;