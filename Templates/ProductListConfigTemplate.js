// ? Product List Configuration Template

// ? _MerchantName_ List Config

// FIXME: add product link field and merchant name field in merchant config and here
// TODO: add ability to add extra static information
// TODO: add ability to generate new data by retrieving data from page and appending some static data to it
// TODO: add ability to add urls for nect page with incrementing page no.
// TODO: add ability to define user defined functions
const _MerchantName_Config = {
    merchant: '',   //? link for merchant
    scope: '', //? css selector for the product container
    // ? 👇 the required main data => should have either id of link field to generate product ids
    // FIXME: rename mainData to mainDetails for better name clearity 
    mainData : {
        title: '', //? required fields
        price: '',  //? @attr to require specific attribute like src, href etc and 
                   // ?   | _filterName_ to apply specific filter but make sure that the filter is there in the filter array
        link: '', 
        image: '',
        // ...
    },
    // ? 👇 the details that needs to be fetched by going to individual product page
    innerDetails : {
        desc: '',
        specs: ''
        // ...
    },
    innerDetailSource: '',    // ? source of individual product page links
    nextPage: '...', // ? source for the link of next page
    nextBtn: '', // ? source for the next page when you have to click button to go to next page
    limit: 1, // ? no of pages to be scrapped and in case for infinite scrolling sites no of items to be scrapped. enter negative number to scrape all the items
    type: 'list',       //? type of information either list of product (list) or individual product information (info)
    mode: 'pupp',       //? mode of scrapping either (xray, pupp, hybrid)
}
module.exports = _MerchantName_Config;