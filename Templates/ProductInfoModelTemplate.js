// ? returns individual product
// ? tailored specifically for puppeteer scrapping

// ? _ModelName_ Info Model

const mongoose = require('mongoose');

// FIXME: add product link field and merchant name field in merchant config and here
// FIXME: for data that doesnt have id so something
// ? this is ideal product info that is required
// ? however many merchants may have data stored in other for so take note and change as required
const _MerchantName_ = {
    schema: {
        // ? 💡 if the product have id then it will be used to generate id or 
       //? 👇 else products link will be used to generate id or else the title will be used to generate id
        _id: String,
        title: String,
        mrp: String,
        sellingPrice: String,
        offers: [String], //not technically offers
        ratingAndReviews: { rating: String, reviews: String },
        category: [String],
        main_image: String,
        image_gallery: [String],
        key_features: [{
            key: String,
            value:  String,
            desc:  String
        }],
        description: String,
        specifications: [{
            groupTitle: String,
            specs: [{
                key: String,
                value: String
            }]
        }],
        color: String, //TOFIX: not all colors are available in this selector
        hardware_spec: String,
        variation: [{
            product_id: String,
            name: String,
            link: String,
        }],
    },
    modelname: '', // ? 📝 Model name is same as collection name with all letters lowercased and an 's' appended to the end
    // 👆 TODO: do something for model name => mongoose converts every name to lowercase and appends an s at the end
}

// TOFIX: generate mongoose model here
const _MerchantName_Schema = new mongoose.Schema(_MerchantName_.schema, { timestamps: true });
const _MerchantName_Model = mongoose.model(_MerchantName_.modelname, _MerchantName_Schema);

module.exports = _MerchantName_Model;