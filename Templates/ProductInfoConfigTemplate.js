// ? Ideal template to generate config for individual product configs
// ? this will not work with pupp unless it have simple selectors i.e. selectors that doesnot co ntains [], x(), {}, etc.

// ? _MerchantName_ Info Config

// FIXME: do something to move this xray block to somewhere else 
const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// ? Donot delete the things that are not there instead comment out so that you know that these information is not there
// FIXME: there is no current product link or merchant name. 
// FIXME: get ability to retrieve id wether it is in url or located somewhere deep in the page 
// TODO: image gallery with foto rama are giving thumbnail.
    // 👆 Change the image address of thumbnail / 90x90(may be different according to merchant) to image / 700x700(may be different according to merchant)
// TODO: add ability to add extra information (either static or dynamic) either from here or from program
// TODO: add ability to get information from url that is provided to this config (to get id of product)
// TODO: add ability to get model no/name of product so that same product from different websites can be compared
const _MerchantName_config = {
    mainData: {
        id: '.product-title .prod-extra p span | trim',
        title: 'h1 | trim | upper',
        mrp: '.cross-price .strike',
        sellingPrice: '.total--sale-price',
        offers: ['.pdp-icon | trim'],
        ratingAndReviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
        category: ['.other:not(:last-child)'],
        main_image: '.mainproduct-slider img@src',
        image_gallery: ['.slider.slider-nav .mpt-item .sdg-ratio img@src'],
        key_features: x('.clearfix.nav.features li', [{
            key: 'strong',
            value: '.h-content',
            desc: '.tooltip@html'
        }]),
        description: '#inpage_container@html',
        specifications: x('.detailspecbox table table', [{
            groupTitle: 'th',
            specs: x('tr:not(:first-child)', [{
                key: 'td.specs-heading',
                value: 'td:not(.specs-heading)'
            }])
        }]),
        color: '.nav.features li:last-child .h-content',
        hardware_spec: '',
        variation: x('.alternative-slider .slide', [{
            product_id: '',
            name: 'img@title',
            link: 'a@href',
        }]),
    },
    type: 'info',               //? type of information either list of product (list) or individual product information (info)
    mode: 'hybrid',            //? mode of scrapping either (xray, pupp, hybrid)
}
module.exports = _MerchantName_config;


/* reference
-----------------------------------------------------------------------
Product
	* id -> extract from url or from HTML 
		-> function is each config to get rhe id 
	* title -> full 
	* MRP -> Optinal 
	* Selling Price -> 
	* Offers -> array of offers
	* rating ( no of votirs and rating) {rating:3.5 , votes:55}
	* Categories ->  array
	* Main Image -> single 
	* Product Images -> array
	* Key Features ( Highlits) -> array (summary) 
	* Description ( HTML )
	* Specifications -> JSON
		Group
			Key => Value
	* Color
	* Hardware Spec
	* Variations
		Product ID /Link 
		Name
        Type { color/spec}

-----------------------------------------------------------------------------
*/