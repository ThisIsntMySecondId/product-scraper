// ? Awok List Config

//                                                                                              👇
//                                                                                             -------
// ? id in url => /mobile-phones/mi_xiaomi_redmi_k20_pro_64gb_6gb_ram_4g_lte_dual_sim_black/dp-1649447 

const awokConfig = {
    merchant: 'https://ae.awok.com/',
    scope: '.productslist_item',
    mainData: {
        // ? id is in the url
        // title: '.productslist_item_title',   //? wont give full title
        title: '.productslist_item_image img@title',
        cost_price: '.productslist_item_priceold', 
        sell_price: '.productslist_item_pricenew', 
        discount: '.productslist_item_banner', 
        link: 'a.productslist_item_link@href', 
        image: '.productslist_item_image img@src',
    },    
    nextPage: '.modern-page-navigation a.modern-page-next@href',
    limit: 3,
    type: 'list',
    mode: 'xray', 
}
module.exports = awokConfig;