// ? sprii List Config

// ? id in '.product-item-inner .product-item-actions form input[name="product"]@value'
const spriiConfig = {
    merchant: 'https://www.sprii.ae/',  
    scope: '.item.product.product-item',
    mainData: {
        id: '.product-item-inner .product-item-actions form input[name="product"]@value',
        title: '.product.name.product-item-name | trim', 
        sell_price: '.price-box.price-final_price .price-container.price-final_price .price | trim',  
        cost_price: '.price-box.price-final_price .old-price .price | trim',  
        link: '.product.name.product-item-name a@href',
        image: '.product-image-photo@src',
    },
    nextPage: '.category-pagination li.active + li a@href', 
    limit: 64, 
    type: 'list',       
    mode: 'puppscroll',       
}
module.exports = spriiConfig;