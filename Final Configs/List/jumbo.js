// ? jumbo List Config

// ? id in '.add-to-cart form input[name=variant_id]@value'

const jumboConfig = {
    merchant: 'https://www.jumbo.ae',
    scope: 'ul#search-result-items li',
    mainData: {
        id: '.add-to-cart form input[name=variant_id]@value',
        title: '.variant-title',
        link: '.variant-title a@href',
        image: 'img@src',
        cost_price: '.variant-list-price | trim',
        sell_price: '.variant-final-price | trim',
        ratings: '.testfreaks-items .testfreaks',
        reviews: '.testfreaks-items .testfreaks@title',
    },
    nextPage: '.pagination a.next_page@href', 
    limit: 2,
    type: 'list',
    mode: 'pupp',
    // mode: 'xray', //? wont give you ratings and reviews 
}
module.exports = jumboConfig;