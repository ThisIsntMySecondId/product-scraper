// ? Microless List Config

// ? id in '@data-prodid' and 
// ? '.product-details-wrap .product-details .compare-checkbox-container input[name="compare"]@value'
// note there is also the sku of product in '.product-details-wrap .product-details .attribute-list ul li' which matches the products sku on info page

const microlessConfig = {
    merchant: 'https://uae.microless.com',
    scope: '.product.grid-list',
    mainData: {
        id: '.product-details-wrap .product-details .compare-checkbox-container input[name="compare"]@value',
        title: '.product-details .product-title | trim | upper',
        sell_price: '.new-price | trim',
        cost_price: '.old-price | trim',
        link: '.product-image a@href',
        image: '.product-image img@data-src',
    },
    nextBtn: '.category-pagination li.active + li a',
    limit: 4,
    type: 'list',
    mode: 'pupp',
}
module.exports = microlessConfig;