// ? Shopkees List Config

// ? id in '.product-item-inner .product-item-actions form input[name="product"]@value'

const shopkeesConfig = {
    merchant: 'https://www.shopkees.com/',
    scope: '.item.product-item',
    mainData: {
        id: '.product-item-inner .product-item-actions form input[name="product"]@value',
        title: '.product-item-details strong | trim',
        cost_price: '.price-box.price-final_price > span:nth-child(2) .price | trim',
        sell_price: '.price-box.price-final_price > span:nth-child(1) .price | trim',
        link: 'a@href',
        image: 'img@src',
     },
    nextPage: '.items.pages-items .item.pages-item-next a@href',
    limit: 3,
    type: 'list',
    mode: 'xray',
}
module.exports = shopkeesConfig;