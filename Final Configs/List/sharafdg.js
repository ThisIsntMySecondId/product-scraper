// ? Sharafdg List Config

// FIXME: id not found 

const sharafdgConfig = {
    merchant: 'https://uae.sharafdg.com',
    scope: '.product-container .slide',
    mainData: {
        title: 'h4',
        cost_priceString: '.product-price .cross-price',
        sell_priceString: '.product-price .price',
        ratings: '.rating-badge',
        discount: '.discount-holder',
        link: 'a@href',
        image: 'img@src',
    },
    limit: -30,
    type: 'list',
    mode: 'puppscroll',
}

module.exports = sharafdgConfig;