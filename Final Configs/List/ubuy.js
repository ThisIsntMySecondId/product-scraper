// ? ubay List Config

// ? ID in '.product-img a span@data-sku'

const ubayConfig = {
    merchant: 'https://www.ubuy.ae',
    scope: '#usstore-products .product-search', 
    mainData: {
        id: '.product-img a span@data-sku',
        title: '.product-body h3',
        sell_price: '.product-price',
        cost_price: '.product-price .product-old-price',
        link: '.product-img a@href',
        image: '.product-img a img@data-src',
    },
    nextBtn: '.pagination li.active + li a',
    limit: 5,
    type: 'list',
    mode: 'pupp',
}
module.exports = ubayConfig;