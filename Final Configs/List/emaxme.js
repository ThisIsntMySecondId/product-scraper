// ? emaxme List Config
// FIXME: every time the search query changes on the same search. as well as when p=2 and q=laptops are reversed results changes, and also the page have infinite scrolling to load more page despite of having pagination button. 
    // 👆 And on opening the links of the pagination buttons opens the page with different products instead of clicking it.

// ? id in product link as well as in '.product-item-inner .actions-primary form input[name="product"]@value'

const emaxmeConfig = {
    merchant: 'https://www.emaxme.com',
    scope: '.item.product.product-item',
    mainData: {
        id: '.product-item-inner .actions-primary form input[name="product"]@value',
        title: '.product.name.product-item-name | trim',
        image: '.product.photo.product-item-photo a img@src',
        link: '.product.name.product-item-name a@href',
        cost_price: '.price-box.price-final_price > span:nth-child(2) .price',
        sell_price: '.price-box.price-final_price > span:nth-child(1) .price',
        // cost_price: '.price-box.price-final_price .old-price .price-final_price .price',
        // sell_price: '.price-box.price-final_price .special-price .price-final_price .price',
        discount: '.product.photo.product-item-photo .product-labels .product-label.sale-label',
    },
    nextPage: '.items.pages-items .item.pages-item-next a@href',
    limit: 3,
    type: 'list',
    mode: 'xray',
}
module.exports = emaxmeConfig;