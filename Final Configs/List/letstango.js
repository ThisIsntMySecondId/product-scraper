// ? Letstango List Config

// FIXME: no id found

const letatangoConfig = {
    merchant: 'https://www.letstango.com',
    scope: '.hits .dealblock_thump',
    mainData : {
        title: 'h1 | capatilize',
        sell_price: 'h2 | trim',
        cost_price: '.oldprice | trim',
        link: '.thump_img a@href',
        image: '.thump_img a img@src | ddd',
    },
    nextPage: '.ais-pagination li.ais-pagination--item__next a',
    limit: 3,
    type: 'list',
    mode: 'pupp',
}
module.exports = letatangoConfig;