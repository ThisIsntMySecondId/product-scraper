// ? godukan List Config
// FIXME: id not found 
const godukanConfig = {
    merchant: 'https://www.godukkan.com',
    scope: '.row.main-products.product-grid .product-grid-item',
    mainData : {
        title: 'h4',
        cost_price: '.price .price-old',    
        sell_price: '.price .price-new',    //FIXME: this sell price is not available for products with single prices 
        sell_price: '.price',               // 👆 it contains the single price but is overwritting the products sell price
        link: 'a@href', 
        image: '.product-thumb .image img@data-src',
    },
    nextPage: '.row.pagination .links ul li.active + li a@href',
    limit: 2,
    type: 'list',
    mode: 'xray',
}
module.exports = godukanConfig;