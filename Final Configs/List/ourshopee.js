// ? Product List Configuration Template

// ? ourshopee List Config

// FIXME: id not found but there is this no => '.actions .add-to-cart a@onclick' which is also in the title of product info page title 
// and sku on product info page is different

const ourshopeeConfig = {
    merchant: 'https://www.ourshopee.com/',
    scope: '.single-product',
    mainData : {
        title: '.prod-info h2',
        cost_price: '.prod-info .price-box .old-price',
        sell_price: '.prod-info .price-box .price',
        link: '.product-image a@href', 
        image: '.product-image a img@src',
    },
    limit: -1,
    type: 'list',
    mode: 'puppscroll',
}
module.exports = ourshopeeConfig;