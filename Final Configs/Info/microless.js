// ? Microless Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

//FIXME: there are variations on this website check them out as well as color and hardware 
// https://uae.microless.com/product/apple-iphone-8-plus-64gb-space-gray-with-screen-protector-mq8d2ll-a/

const microlessConfig = {
    mainData: {
        id: '.product-additional-info .sku | trim', //? sku
        title: 'h1.product-title-h1 | trim',
        mrp: '.product-price-wrapper .product-price-old .amount-fig | trim',
        sellingPrice: '.product-price-wrapper .product-price | trim',
        // offers: ['.pdp-icon | trim'],
        // ratingAndReviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
        category: ['.breadcrumb li:not(:first-child):not(:last-child) | trim'],
        main_image: 'meta[property="og:image"]@content',
        image_gallery: ['#product-images-slider img@src'],
        // key_features: x('.clearfix.nav.features li', [{
        //     key: 'strong',
        //     value: '.h-content',
        //     desc: '.tooltip@html'
        // }]),
        description: '.product-description@html | trim',
        specifications: x('.product-attribute-row',[{
                key: '.col-md-3 | trim',
                value: '.col-md-9 | trim'
        }]),
        // color: '.nav.features li:last-child .h-content',
        // hardware_spec: '',
        // variation: x('.alternative-slider .slide', [{
        //     product_id: '',
        //     name: 'img@title',
        //     link: 'a@href',
        // }]),
    },
    type: 'info',
    // mode: 'hybrid',
    mode: 'xray',
}
module.exports = microlessConfig;