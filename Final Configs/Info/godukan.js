// ? Godukan Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// FIXME: add merchant name and link 

const godukanConfig = {
    mainData: {
        id: 'meta[itemprop="sku"]@content',  //?sku
        modelNo: 'ul.description span.p-model',
        brand: 'ul.description li.p-brand a',
        title: 'h1.heading-title | trim',
        mrp: '.price .price-old',
        sellingPrice: 'meta[itemprop="price"]@content', //? alternative .price .price-new but it will not give price on products with only one price
        // offers: ['.pdp-icon | trim'],
        // ratingAndReviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
        category: ['ul.breadcrumb li:not(:first-child):not(:last-Child)'],
        main_image: '#image@src',
        image_gallery: ['#product-gallery a@href'],         //? this will give you full scale image not the image tag
        // key_features: x('.clearfix.nav.features li', [{
        //     key: 'strong',
        //     value: '.h-content',
        //     desc: '.tooltip@html'
        // }]),
        description: '#tab-description@html',
        specifications: x('#tab-specification table tr tr', [{
                key: 'td:nth-child(1)',
                value: 'td:nth-child(2)'
        }]),
        // color: '.nav.features li:last-child .h-content',
        // hardware_spec: '',
        // variation: x('.alternative-slider .slide', [{
        //     product_id: '',
        //     name: 'img@title',
        //     link: 'a@href',
        // }]),
    },
    type: 'info',
    mode: 'xray',
}
module.exports = godukanConfig;