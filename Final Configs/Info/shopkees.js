// ? shopkees Info Config

// FIXME: do something to move this xray block to somewhere else 
const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// ? Donot delete the things that are not there instead comment out so that you know that these information is not there
const shopkeesConfig = {
    mainData: {
        // id: '.product-title .prod-extra p span | trim',
        title: '.product-info-main h1.page-title | trim',
        mrp: '.product-info-main .price-box.price-final_price > span:nth-child(2) .price',
        sellingPrice: '.product-info-main .price-box.price-final_price > span:nth-child(1) .price',
        // offers: ['.pdp-icon | trim'],                //? 👇 out of 100 in %
        ratingAndReviews: { rating: '.product-info-main .product-reviews-summary .rating-summary span[itemprop="bestRating"]', reviews: '.product-info-main .product-reviews-summary .reviews-actions span[itemprop="reviewCount"] | trim' },
        category: ['.breadcrumbs li:not(:first-child):not(:last-child)'],
        main_image: '.fotorama__stage__frame.fotorama__active img@src',
        image_gallery: ['.fotorama__nav__shaft img@src'],  //FIXME: gives thumbnail small images //FIXME: images in some of links are not retrieving 
        key_features: '.product.attribute.overview',
        description: '.product.attribute.description@html',
        specifications: x('.additional-attributes-wrapper.table-wrapper tbody tr', [{
                key: '.col.label',
                value: '.col.data'
        }]),
        // color: '.nav.features li:last-child .h-content',
        // hardware_spec: '',
        // variation: x('.alternative-slider .slide', [{
        //     product_id: '',
        //     name: 'img@title',
        //     link: 'a@href',
        // }]),
    },
    type: 'info',
    mode: 'xray',
}
module.exports = shopkeesConfig;