// ? Emaxme Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// FIXME: rename id to sku 

const emaxmeConfig = {
    mainData: {
        id: '.product-info-main .product-info-stock-sku div[itemprop="sku"]',       //SKU no
        modelNo: '.product-info-main .product-info-stock-sku div[itemprop="model_mob "]', //Product Model No
        title: 'h1.page-title | trim',
        merchant: ' | default:emaxme',
        mrp: '.price-box.price-final_price > span:nth-child(2) .price',
        sellingPrice: '.price-box.price-final_price > span:nth-child(1) .price',
        // offers: ['.pdp-icon | trim'],
        // ratingAndReviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
        // category: ['.other:not(:last-child)'],
        main_image: '.fotorama__stage__frame.fotorama__active img@src',
        image_gallery: ['.fotorama__nav__shaft img@src'],
        key_features: ['.product.attribute.overview li'],
        description: '.product.attribute.description@html',         //FIXME: not giving full html code 
        specifications: x('.data.table.additional-attributes tbody tr', [{
            key: 'th',
            value: 'td'
        }]),
        // color: '.nav.features li:last-child .h-content',
        // hardware_spec: '',
        // variation: x('.alternative-slider .slide', [{
        //     product_id: '',
        //     name: 'img@title',
        //     link: 'a@href',
        // }]),
    },
    type: 'info',
    mode: 'hybrid',
    // mode: 'xray', //Doesnt gives main-image and image-gallery
}
module.exports = emaxmeConfig;
