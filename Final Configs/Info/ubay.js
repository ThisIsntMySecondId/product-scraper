// ? Ubay Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// FIXME: there is no current product link or merchant name. 
const ubayConfig = {
    mainData: {
        id: '.product-details #item_id',
        title: 'h2.product-name | trim',
        mrp: '.product-details .product-price del',
        sellingPrice: '.product-details .product-price span[itemprop="price"]@content',
        // offers: ['.pdp-icon | trim'],
        // ratingAndReviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
        category: ['.breadcrumbs li:not(:first-child):not(:last-Child)'],
        main_image: '#product-main-images .owl-stage .owl-item.active a@href',
        image_gallery: ['#product-thumb-images .owl-stage .owl-item.active a@href'],
        key_features: ['[itemprop="description"] ul li'],
        description: '#product-tab .tab-content #tab2@html',
        specifications: x('#additional-info table tr', [{
                key: 'td:nth-child(1)',
                value: 'td:nth-child(2)'
        }]),
        // color: '',       //FIXME: not all have same default specs some have processo info some have capacity some have size and processor come only have color some have color and storage
        // hardware_spec: '',
        variation: x('#product-variations label', [{
            variationParam: 'span',
            variationValues: x('select option', [{
                name: '@value',
                asin: '@asin'
            }])
        }]),
    },
    type: 'info',               //? type of information either list of product (list) or individual product information (info)
    mode: 'hybrid',            //? mode of scrapping either (xray, pupp, hybrid)
}
module.exports = ubayConfig;