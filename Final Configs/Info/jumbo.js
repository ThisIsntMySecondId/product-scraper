// ? _MerchaneName_ Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// FIXME: rename id to model no

const _MerchantName_config = {
    mainData: {
        // id: '#brand_wrapper #item_sku | trim',
        // id: 'meta[itemprop="productID"]@content | trim',
        id: 'meta[itemprop="sku"]@content | trim',
        model: 'span[itemprop="model"] | trim',
        merchantName: ' | default:jumbo' ,
        title: 'h1 | trim',
        mrp: '.list_price.strike',
        sellingPrice: '.our_price | trim',
        // offers: ['.pdp-icon | trim'],
        ratingAndReviews: { rating: '.tf-rating | trim', reviews: '.tf-count | trim' },
        category: ['.bread-crumbs div:not(:first-child)'],
        main_image: '#catalog-images img@src',
        image_gallery: ['.pdp-thumbnail img@src'],  //FIXME: see if the urls provided are not for thumbnails but for big images
        description: '.pdp-tabs .tab-pane #description@html',
        specifications: x('#feature_groups table tr', [{
            key: ':not(.feature_name)',
            value: '.feature_name'
        }]),  // FIXME Feature goroup title is not comming because the title is not inside the table containing the feature group details 
        color: '.catalog-options ul:nth-child(4) li.selected .catalog-option-title',
        hardware_spec: '.catalog-options ul:nth-child(2) li.selected .catalog-option-title',
        variation: x('.catalog-options', {
            storage: ['ul:nth-child(2) li .catalog-option-title'],
            colors: ['ul:nth-child(4) li .catalog-option-title'],
        }),  //FIXME: links to these options are not available
    },
    type: 'info',
    mode: 'hybrid',
    // mode: 'xray',  //? color, hardware spec, ratings n reviews not available
}
module.exports = _MerchantName_config;

// x('#feature_groups table', [{
//     specTitle: 'tr ~ h4',
//     spec: x('tr', [{
//         key: ':not(.feature_name)',
//         value: '.feature_name'
//     }])
// }])