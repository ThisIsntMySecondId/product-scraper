// ? Awok Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// FIXME: addmerchant name and product url

const awokConfig = {
    mainData: {
        id: '.prod_detail_cart_info .buy_now_btn a@data-pid | trim',  //? try to get this from url
        title: 'h1 | trim',
        mrp: '.prod_detail_cart_prices .old_price',
        sellingPrice: '.prod_detail_cart_prices .new_price',
        // offers: ['.pdp-icon | trim'],
        // ratingAndReviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' }, //? reconsider it even though they have no ratings and reviews they have place to submit them
        category: ['.page_breadcrums_box ul li[itemtype]:not(:first-child)'],
        main_image: '.prod_preview .main img@src',
        image_gallery: ['.owl-controls .owl-dots .owl-dot@data-src'],
        // key_features: x('.clearfix.nav.features li', [{
        //     key: 'strong',
        //     value: '.h-content',
        //     desc: '.tooltip@html'
        // }]),
        description: '.product_content_wrapper #tab2 .product_block@html',  //? even though most of them contains similar data, some products such as microwave oven contains some product related information.
        specifications: x('.product_content_wrapper #tab2 UL.product_specs li', [{
                key: 'li > b',
                value: 'li > span'
        }]),
        color: '.pd_info_colors a.selected',
        hardware_spec: '.pd_info_storage a.selected',
        variation: {
            storage: x('.pd_info_storage a', [{
                size: '',
                link: '@href'
            }]),
            colors: x('.pd_info_colors a', [{
                size: '',
                link: '@href'
            }]),
        },
    },
    type: 'info',
    mode: 'xray',
}
module.exports = awokConfig;