// ? Sharafdg Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// FIXME: Add merchant name
// FIXME: Add model no annd sku no

const sharafdgConfig = {
    mainData: {
        id: '.product-title .prod-extra p span | trim',
        title: 'h1 | trim | upper',
        mrp: '.cross-price .strike',
        sellingPrice: '.total--sale-price',
        offers: ['.pdp-icon | trim'],
        ratingAndReviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
        category: ['.other:not(:last-child)'],
        main_image: '.mainproduct-slider img@src',
        image_gallery: ['.slider.slider-nav .mpt-item .sdg-ratio img@src'],
        key_features: x('.clearfix.nav.features li', [{
            key: 'strong',
            value: '.h-content',
            desc: '.tooltip@html'
        }]),
        description: '#inpage_container@html',
        specifications: x('.detailspecbox table table', [{
            groupTitle: 'th',
            specs: x('tr:not(:first-child)', [{
                key: 'td.specs-heading',
                value: 'td:not(.specs-heading)'
            }])
        }]),
        color: '.nav.features li:last-child .h-content',
        hardware_spec: '',
        variation: x('.alternative-slider .slide', [{
            product_id: '',
            name: 'img@title',
            link: 'a@href',
        }]),
    },
    type: 'info',
    mode: 'hybrid',
    // mode: 'xray'  //? this wont give description
}
module.exports = sharafdgConfig;