// ? sprii Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// FIXME: Add merchant name

const spriiConfig = {
    mainData: {
        id: 'td[data-th]',
        title: 'h1 | trim',
        mrp: '.old-price | trim',
        sellingPrice: '.price-box.price-final_price | trim',
        category: ['.breadcrumbs li:not(:first-child):not(:last-Child) | trim'],
        main_image: '.fotorama__stage__frame.fotorama__active img@src',
        image_gallery: ['.fotorama__nav__shaft img@src'],
        key_features: x('table tr', [{
            key: 'th.label',
            value: 'td.data',
        }]),
        description: '.description@html',
        // FIXME: specification jumbles with description 
    },
    type: 'info',
    // mode: 'xray',            //? it doesnot gives images
    mode: 'hybrid',            //? it gives image urls
}
module.exports = spriiConfig;