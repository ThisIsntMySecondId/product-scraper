// ? Letatango Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// FIXME: Add merchant name

const letatangoConfig = {
    mainData: {
        // id: '.product-title .prod-extra p span | trim', //FIXME: id cannot be found anywhere on page 
        title: 'h1 | trim',
        mrp: '.product-price-block .oldprice',
        sellingPrice: '.product-price-block .selling-price | trim',
        // offers: ['.pdp-icon | trim'],
        // ratingAndReviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
        category: ['.pagecrumb li:not(:first-child):not(:last-child) | trim'],
        main_image: '.product-gallery img@src',
        image_gallery: ['.product-gallery-nav img@src'],
        key_features: ['.short-description-block ul li'],
        description: '.overview@html',
        specifications: x('.table-row', [{
            key: '.table-cell.title',
            value: '.table-cell.content | trim',
        }]),
        color: '.dropdown-container button | trim',
        hardware_spec: '.dropdown-container:nth-child(2) button | trim',
        variation: x('.dropdown-container', [{
            variationParam: 'p | trim',
            variations: x('ul li', [{
                value: 'a',
                link: 'a@href'
            }])
        }]),
    },
    type: 'info',
    mode: 'xray',
}
module.exports = letatangoConfig;