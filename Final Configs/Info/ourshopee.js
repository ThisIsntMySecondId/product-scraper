// ? Ourshopee Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

const ourshopeeConfig = {
    mainData: {
        id: '.prod-info tr:first-child td:nth-child(3)',
        title: 'h1',
        mrp: '.prod-info .price-box .price | trim',
        sellingPrice: '.prod-info .price-box .old-price | trim',
        // offers: ['.pdp-icon | trim'],
        ratingAndReviews: { rating: '.rating .rating-container input@value', reviews: '.rating | trim' },
        category: ['.bread-crumb li:not(:first-child):not(:last-Child)'],
        main_image: '.ubislider-image-container img@src',
        image_gallery: ['.ubislider-inner img@src'],
        key_features: x('.prod-info table tr', [{
            key: 'td:nth-child(1)',
            value: 'td:nth-child(3)',
        }]),
        description: '#pr-description p:first-child',
        specifications: x('#pr-description table tr', [{
                key: 'td:nth-child(1)',
                value: 'td:nth-child(2)'
        }]),
        color: '.extra-dts-panel .row:nth-child(1) > div:nth-child(2) a:not([href])@title',
        hardware_spec: '.extra-dts-panel > .row:nth-child(2) > div:nth-child(2) a:not([href])',
        variation: {
            color: x('.extra-dts-panel > .row:nth-child(1) > div:nth-child(2) a', [{
                name: '@title',
                link: '@href'
            }]),
            storage: x('.extra-dts-panel > .row:nth-child(2) > div:nth-child(2) a', [{
                name: '',
                link: '@href'
            }]),
        },
    },
    type: 'info',
    mode: 'xray',
}
module.exports = ourshopeeConfig;