// ? here all the configuration including and validation takes place after which the data is passed to core.js
const retrieveAndStore = require('../core/core').retrieveAndStore;
// TODO: test each merchant info and list
// ? Main function
(async () => {
    // 🔔 sprii 🔔
    // ? test with sprii list
    // const config = require('../Final Configs/List/sprii');
    // const url = 'https://www.sprii.ae/en/appliances/?p=1';
    // // let data = await retrieveAndStore(url, config);
    // // console.log(data);
    // // // console.log(data.length);
    // // // // ? storing to db
    // const model = require('../Final Models/List/sprii');
    // await retrieveAndStore(url, config, model);
    // ? test with sprii info
    // const config = require('../Final Configs/Info/sprii');
    // const url = 'https://www.sprii.ae/en/smeg-50-s-retro-style-single-door-refrigerator-pastel-blue.html';
    // // const data = await retrieveAndStore(url, config);
    // // console.log(data);
    // // ? store to db
    // const model = require('../Final Models/Info/sprii');
    // await retrieveAndStore(url, config, model);

    // 🔔 letstango 🔔
    // ? test with letstango list
    // const config = require('../Final Configs/List/letstango');
    // const url = 'https://www.letstango.com/?q=dell%20laptops&idx=letsTango_default_products&p=0&is_v=1';
    // // console.log(await retrieveAndStore(url, config));
    // const model = require('../Final Models/List/letstango');
    // await retrieveAndStore(url, config, model);
    // ? test with letstango info
    // const config = require('../Final Configs/Info/letatango');
    // // const url = 'https://www.letstango.com/dell-xps-15-touch-laptop-core-i9-8950hk-32gb-ram-2tb-ssd-nvidia-gtx-1050ti-with-4gb-gddr5-win10-pro-silver'
    // const url = 'https://www.letstango.com/apple-iphone-11-pro-max-512gb-gold-without-facetime';
    // // const url = 'https://www.letstango.com/samsung-galaxy-note-10-smartphone-lte-512gb-aura-black';
    // const fs = require('fs');
    // fs.createWriteStream('./temp.json').write(JSON.stringify(await retrieveAndStore(url, config)));
    // // fs.createWriteStream('./temp2.json').write(JSON.stringify(await retrieveAndStore(url, config)));
    // const model = require('../Final Models/Info/letstango');
    // await retrieveAndStore(url, config, model);

    // 🔔 awok 🔔
    // => Website becomes unresponsive sometimes
    // ? test with awok list
    // const config = require('../Final Configs/List/awok');
    // // const url = 'https://ae.awok.com/mobile-phones/ds-582/';
    // // const url = 'https://ae.awok.com/mobile-phones/ds-582/?brand=apple&set-filter=Y';
    // const url = 'https://ae.awok.com/Laptops-Notebooks/ds-744/';
    // // const data = await retrieveAndStore(url, config);
    // // console.log(data);
    // // console.log(data.length);
    // const model = require('../Final Models/List/awok');
    // await retrieveAndStore(url, config, model);
    // ? test with awok info
    // const config = require('../Final Configs/Info/awok');
    // // const url = 'https://ae.awok.com/mobile-phones/apple_iphone_11_pro_max_6_5_inch_display_512gb_gold/dp-1625121/';
    // // const url = 'https://ae.awok.com/mobile-phones/mi_xiaomi_redmi_k20_pro_64gb_6gb_ram_4g_lte_dual_sim_black/dp-1649447/';
    // // const url = 'https://ae.awok.com/Laptops-Notebooks/acer_chromebook_c720_intel_celeron_2955u_1_4_ghz_processor_2gb_ram_32gb_ssd_11_6_inch_led_display/dp-1635274/';
    // const url = 'https://ae.awok.com/mobile-phones/gaho_g3000_dual_sim_dual_cam_5_5_ips_gold/dp-1649443/';
    // // const url = ''
    // // console.log(await retrieveAndStore(url, config));
    // const fs = require('fs');
    // fs.createWriteStream('./temp.json').write(JSON.stringify(await retrieveAndStore(url, config)));
    // const model = require('../Final Models/Info/awok');
    // await retrieveAndStore(url, config, model);

    // 🔔 sharafdg 🔔
    // ? test with sharafdg list
    // => they use algolia api whose data can be retrieved => tried in postman
    // const url = 'https://uae.sharafdg.com/?q=iphone%2010&post_type=product';
    // const config = require('../Final Configs/List/sharafdg');
    // // console.log(await retrieveAndStore(url, config));
    // const model = require('../Final Models/List/sharafdg');
    // await retrieveAndStore(url, config, model);
    // ? test with sharafdg info
    // const config = require('../Final Configs/Info/sharafdg');
    // // const url = 'https://uae.sharafdg.com/product/apple-iphone-11-128gb-black/';
    // const url = 'https://uae.sharafdg.com/product/apple-airpods-with-wireless-charging-case/';
    // // const url = 'https://uae.sharafdg.com/product/samsung-galaxy-note10-256gb-aura-white-sm-n970f-4g-dual-sim-smartphone/';
    // // console.log(await retrieveAndStore(url, config));
    // const model = require('../Final Models/Info/sharafdg');
    // await retrieveAndStore(url, config, model);

    // 🔔 jumbo 🔔
    // ? test with jumbo list
    // const config = require('../Final Configs/List/jumbo');
    // const url = 'https://www.jumbo.ae/home/search?q=apple+iphone+10';
    // const url = 'https://www.jumbo.ae/home/search?q=laptops';
    // const url = 'https://www.jumbo.ae/home/search?q=iphone+11';
    // console.log(await retrieveAndStore(url, config));
    // console.log((await retrieveAndStore(url, config)).length);
    // const model = require('../Final Models/List/jumbo');
    // await retrieveAndStore(url, config, model);
    // ? test with jumbo info
    // const url = 'https://www.jumbo.ae/smart-phones/apple-iphone-xs-smartphone-lte-with-facetime/p-0441617-25953993304-cat.html#variant_id=0441617-8394598935';
    // // const url = 'https://www.jumbo.ae/smart-phones/samsung-galaxy-note-10-smartphone-lte/p-0441617-25874860577-cat.html#variant_id=0441617-65305733523';
    // // const url = 'https://www.jumbo.ae/smart-phones/apple-iphone-11-4g-lte-smartphone/p-0441617-19151979293-cat.html#variant_id=0441617-16099707222';
    // // const url = 'https://www.jumbo.ae/coffee-makers/nespresso-f521-lattissima-touch-coffee-machine-black/p-0441617-89039968231-cat.html#variant_id=0441617-89039968231';
    // const config = require('../Final Configs/Info/jumbo');
    // console.log(await retrieveAndStore(url, config));
    // const fs = require('fs');
    // fs.createWriteStream('./temp.json').write(JSON.stringify(await retrieveAndStore(url, config)));
    // const model = require('../Final Models/Info/jumbo');
    // await retrieveAndStore(url, config, model);

    // 🔔 emaxme 🔔
    // ? test with emaxme list
    // const url = 'https://www.emaxme.com/s001/shop-by-catagory/computer-and-laptop.html';
    // const config = require('../Final Configs/List/emaxme');
    // const data = await retrieveAndStore(url, config);
    // // // const fs = require('fs');
    // // // fs.createWriteStream('./xtemp4.json').write(JSON.stringify(data));
    // console.log(data);
    // const model = require('../Final Models/List/emaxme');
    // await retrieveAndStore(url, config, model);
    // ? test with emaxme info
    // // const url = 'https://www.emaxme.com/s001/catalog/product/view/id/10486/s/iphone-11-64gb-black-pre-order-1/';
    // // const url = 'https://www.emaxme.com/s001/catalog/product/view/id/9045/s/samsung-galaxy-a30-4gb-ram-64gb-blue/category/592/';
    // // const url = 'https://www.emaxme.com/s001/catalog/product/view/id/10373/s/apple-macbook-pro-with-touch-bar-2019-15-9th-gen-2-6ghz-i7-16gb-ram-256gb-ssd-arabic-english-key-board-space-grey/';
    // const url = 'https://www.emaxme.com/s001/catalog/product/view/id/9418/s/samsung-49-uhd-smart-flat-tv-ua49ru7100kxzn/category/672/';
    // // // // const url = '';
    // const config = require('../Final Configs/Info/emaxme')
    // const data = await retrieveAndStore(url, config);
    // const fs = require('fs');
    // fs.createWriteStream('./temp.json').write(JSON.stringify(data));
    // console.log(await retrieveAndStore(url, config));
    // const model = require('../Final Models/Info/emaxme');
    // await retrieveAndStore(url, config, model)

    // 🔔 ubuy 🔔
    // ? test with ubuy list
    // FIXME: doesnt gives accurate results everytime because of hardcoded 5000ms wait after buton click 
    // ? uses lazy loading for images need to use pupp scroll to specific element
    // const url = 'https://www.ubuy.ae/en/search/?ref_p=ser_tp&q=laptops';
    // const url = 'https://www.ubuy.ae/en/search/?ref_p=ser_tp&q=laptops&brand=HP';
    // const url = 'https://www.ubuy.ae/en/search/?ref_p=ser_tp&q=laptops&brand=Dell';
    // const url = 'https://www.ubuy.ae/en/cell-phones-accessories/mobiles/id-7072561011/category-list-view/';
    // const url = 'https://www.ubuy.ae/en/home-furniture/id-1055398/category-list-view/';
    // const config = require('../Final Configs/List/ubuy');
    // let data = await retrieveAndStore(url, config);
    // console.log(data);
    // // const fs = require('fs');
    // // fs.createWriteStream('./temp5.json').write(JSON.stringify(data));
    // const model = require('../Final Models/List/ubay');
    // await retrieveAndStore(url, config, model);
    // ? test with ubuy info
    // const config = require('../Final Configs/Info/ubay');
    // // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07Q1NBCVB/s/new-apple-imac-27-inch-retina-5k-display-3-0ghz-6-core-8th-generation-intel-core-i5-processor-1tb/store/store';
    // // const url = 'https://www.ubuy.ae/en/search/index/view/product/B079ZJLMJN/s/dell-inspiron-15-6-inch-hd-touchscreen-flagship-high-performance-laptop-pc-intel-core-i5-7200u-8gb-ram-256gb-ssd-bluetooth-wifi-windows-10-black/store/store';
    // // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07PXGQC1Q/s/apple-airpods-with-charging-case-latest-model/store/store';
    // // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07D1CPRBN/s/apple-iphone-6s-plus-64gb-rose-gold-fully-unlocked-renewed/store/store';
    // // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07NQRSZ2F/s/apple-iphone-6s-32gb-gold-fully-unlocked-renewed/store/store';
    // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07J215RPT/s/samsung-chromebook-plus-v2-2-in-1-4gb-ram-64gb-emmc-13mp-camera-chrome-os-12-2-16-10-aspect-ratio-light-titan-xe520qab-k03us/store/store';
    // // console.log(await retrieveAndStore(url, config));
    // // const fs = require('fs');
    // // fs.createWriteStream('./temp12.json').write(JSON.stringify(await retrieveAndStore(url, config)));
    // const model = require('../Final Models/Info/ubuy');
    // await retrieveAndStore(url, config, model);

    // 🔔 microless 🔔
    // => they have api tried in postman
    // ? test with microless list 
    // const url = 'https://uae.microless.com/notebooks/hp/b/l/';
    // const config = require('../Final Configs/List/microless');
    // let data = await retrieveAndStore(url, config);
    // console.log(data);
    // console.log(data.length);
    // // const fs = require('fs');
    // // fs.createWriteStream('./temp8.json').write(JSON.stringify(data));
    // const model = require('../Final Models/List/microless');
    // await retrieveAndStore(url, config, model);
    // ? test with microless info
    // const url = 'https://uae.microless.com/product/apple-iphone-xs-64gb-silver-with-facetime-4g-lte/';
    const url = 'https://uae.microless.com/product/apple-iphone-8-plus-64gb-space-gray-with-screen-protector-mq8d2ll-a/';
    // const url = 'https://uae.microless.com/product/hp-spectre-x360-13th-touch-convertible-laptop-intel-quad-core-i7-8500u-1-6ghz-13-3-inch-fhd-touchscreen-16gb-ram-512gb-ssd-intel-uhd-graphics-620-windows-10-hp-sleeve-active-pen-included-pale-rose-gol/';
    const config = require('../Final Configs/Info/microless');
    // // console.log(await retrieveAndStore(url, config));
    const fs = require('fs');
    fs.createWriteStream('./temp.json').write(JSON.stringify(await retrieveAndStore(url, config)));
    // const model = require('../Final Models/Info/microless');
    // await retrieveAndStore(url, config, model);

    // 🔔 shopkees 🔔
    // ? test with shopkees list
    // const config = require('../Final Configs/List/shopkees');
    // const url = 'https://www.shopkees.com/catalogsearch/result/?q=laptops';
    // // // const url = 'https://www.shopkees.com/laptops/business-laptops.html';
    // // console.log(await retrieveAndStore(url, config));
    // const model = require('../Final Models/List/shopkees');
    // await retrieveAndStore(url, config, model);
    // ? test with shopkees info
    // const config = require('../Final Configs/Info/shopkees');
    // // const url = 'https://www.shopkees.com/lenovo-thinkpad-e480-i7-8th-gen-8gb-1tb-14-2gb-win-10-pro-20kn0002ad.html';
    // // const url = 'https://www.shopkees.com/hp-desktop-pro-g2-intel-core-i3-8100-4gb-1tb-dos.html';
    // // const url = 'https://www.shopkees.com/adata-c906-8gb-usb-2-0-flash-drive.html';
    // // console.log(await retrieveAndStore(url, config));
    // const model = require('../Final Models/Info/shopkees');
    // await retrieveAndStore(url, config, model);

    // 🔔 godukan 🔔
    // ? test with godukan list
    // const config = require('../Final Configs/List/godukan');
    // const url = 'https://www.godukkan.com/laptops-computers/laptops/laptop-by-brand/lenovo-234';
    // // console.log(await retrieveAndStore(url, config));
    // // console.log((await retrieveAndStore(url, config)).length);
    // const model = require('../Final Models/List/godukan');
    // await retrieveAndStore(url, config, model);
    // ? test with godukan info
    // const config = require('../Final Configs/Info/godukan');
    // const url = 'https://www.godukkan.com/microsoft/microsoft-surface-pro-4-th9-00003-128-gb-wifi-only-silver-785412.html';
    // // // const url = 'https://www.godukkan.com/laptops/lenovo-120s-81A4007BAK-intel-celeron-4gb-500gb-11.6inch-MS-Dos-grey';
    // // // const url = 'https://www.godukkan.com/samsung-galaxy-note-10-plus-dual-sim-12gb-ram-256gb-4g-lte-smartphone-aura-white?search=Note%2010%20Plus%20256GB%20White&description=true';
    // // // const url = 'https://www.godukkan.com/xiaomi-mi-9t-dual-sim-6gb-ram-128gb-4g-lte-red-flame-global-version?search=Mi%209T&description=true';
    // console.log(await retrieveAndStore(url, config));
    // const model = require('../Final Models/Info/godukan');
    // await retrieveAndStore(url, config, model);

    // 🔔 ourshopee 🔔
    // ? test with ourshopee list
    // const url = 'https://www.ourshopee.com/brands/Apple/5/';
    // // const url = 'https://www.ourshopee.com/products-category/Laptops-Notebooks/';
    // const config = require('../Final Configs/List/ourshopee');
    // // console.log(await retrieveAndStore(url, config));
    // const model = require('../Final Models/List/ourshopee');
    // await retrieveAndStore(url, config, model);
    // ? test with ourshopee info
    // const url = 'https://www.ourshopee.com/product-description/Apple-iPhone-11-Without-FaceTime-Red-128GB-4G-LTE/';
    // const url = 'https://www.ourshopee.com/product-description/Xiaomi-Mi-Max-3-Dual-SIM-64-GB-4-GB-RAM-4G-LTE-Black-Global-Version-/';
    // const url = 'https://www.ourshopee.com/product-description/Lenovo-Ideapad-100S-11IBY-Notebook-116-Inch-HD-Display-Intel-Atom-Quad-Core-Processor-2GB-RAM-32GB-Storage-Windows-10-home-Red/';
    // const config = require('../Final Configs/Info/ourshopee');
    // console.log(await retrieveAndStore(url, config));
    // const fs = require('fs');
    // fs.createWriteStream('./temp7.json').write(JSON.stringify(await retrieveAndStore(url, config)));
    // const model = require('../Final Models/Info/ourshopee');
    // await retrieveAndStore(url, config, model);

    // 🔔 APIs 🔔
    // ? test noon api
    // ? test amazon api
    // ? test ebay api
    // ? test aliexpress api
    // ? test algolia api for letstango, sharafdg, awok
})();