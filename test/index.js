const xrayRetrieveProductInfo = require('../core/scrappers/xrayProduct').xrayRetrieveProductInfo;
const xrayRetrieveProductsList = require('../core/scrappers/xrayProductsList').xrayRetrieveProductsList;
const puppRetrieveProductInfo = require('../core/scrappers/puppProduct').puppRetrieveProductInfo;
const puppRetrieveProductsList = require('../core/scrappers/puppProductsList').puppRetrieveProductsList;
const hybridRetrieveProductInfo = require('../core/scrappers/hybridProduct').hybridRetrieveProductInfo;
const hybridRetrieveProductsList = require('../core/scrappers/hybridProductsList').hybridRetrieveProductsList;

// ? Main function
(async () => {
    // 🔔 XRAYS 🔔
    // ? (single) product info
    // const config = require('../merchant configs/Product Info/sharafdgProductHybrid');
    // const config = require('./configs/Product Info/letstangoProduct');
    // console.log(config);
    // let url = config.url;
    // let data = await xrayRetrieveProductInfo(url, config);
    // console.log("data");

    // ? product list
    // const config = require('./configs/Products List/letatengoList'); //? dynamic, wont work
    // const config = require('./configs/Products List/microlessList');
    // const config = require('../merchant configs/Products List/microlessWithInnerDetails');
    // const config = require('./configs/Products List/myntraList'); //? dynamic, wont work
    // const config = require('./configs/Products List/shopkees');
    // const config = require('../merchant configs/Products List/sprii');
    // console.log(config);
    // let url = config.url;
    // let data = await xrayRetrieveProductsList(url, config);
    // console.log(Array.isArray(data));

    // 🔔 PUPPETEER 🔔
    // ? (single) product info
    // const config = require('./configs/Product Info/myntraProduct');
    // const config = require('./configs/Product Info/LetstangoProductPupp');
    // console.log(config);
    // let url = config.url;
    // let data = await puppRetrieveProductInfo(url, config);
    // console.log(data);

    // ? product list
    // const config = require('./configs/Products List/letatengoList');
    // const config = require('./configs/Products List/microlessList');
    // const config = require('./configs/Products List/microlessWithInnerDetails'); //? wont give inner details though
    // const config = require('./configs/Products List/myntraList');
    // const config = require('./configs/Products List/myntraListWithInnerDetails'); //? wont give inner details though
    // const config = require('./configs/Products List/sprii');
    // console.log(config);
    // let url = config.url;
    // let data = await puppRetrieveProductsList(url, config);
    // console.log(data);

    // 🔔 HYBRID 🔔
    // ? (single) product info
    // const config = require('./configs/Product Info/letstangoProductHybrid');
    // const config = require('./configs/Product Info/sharafdgProductHybrid');
    // const config = require('./configs/Product Info/myntraProduct');
    // console.log(config);
    // let url = config.url;
    // let data = await hybridRetrieveProductInfo(url, config);
    // console.log(data);

    // ? product list
    // const config = require('./configs/Products List/microlessWithInnerDetails'); //? works same as pupp scrapper right now
    // const config = require('../merchant configs/Products List/myntraList'); //? works same as pupp scrapper right now
    // console.log(config);
    // config.limit = 1;
    // let url = config.url;
    // let data = await hybridRetrieveProductsList(url, config);
    // console.log(data);

    // 🔔 db insert 🔔
    // const config = require('../merchant configs/Products List/microlessWithInnerDetails');
    // console.log(config);
    // let url = config.url;
    // let data = await xrayRetrieveProductsList(url, config);
})();

