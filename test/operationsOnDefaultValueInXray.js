const xray = require('x-ray');
const x = xray({
    filters: {
        default: (str, value) => str || value,
        tolower: str => str.toLowerCase(),
        getid: str => str.split('?')[1].split('&')[1].split('=')[1]
    }
});

// ? main
(async () => {
    const someOtherUrl = 'ThisIsSomeThingInUrl/?p=215&id=124578'
    const url = 'https://en.wikipedia.org/wiki/Gabe_Newell';
    //// console.log(await x(url, 'h1'));
    //// const data = await x(url, ['#toc li']);
    //// const data2 = await x(url, '#toc li.nothing | default:ThisIsSomeThing/?p=215&id=124578 | tolower | getid');
    //// const data3 = await x(url, ' | default:ThisIsSomeThing/?p=215&id=124578 | tolower | getid');
    //// const data4 = await x(url, ` | default:${someOtherUrl} | getid`);
    //// console.log('1 =>', data);
    //// console.log('2 =>', data2);
    //// console.log('2 =>', data3);
    //// console.log('2 =>', data4);
    const otherStr = "Not Just WikiPedia";
    const FinalObj = await x(url, {
        title: 'h1',
        [await x('h1')]: ' | default:*** | tolower',
        url: ` | default:${url}`,
        merchantname: ' | default:"Wikipedia Organization"',
        merchantname2: ` | default:"${"Wikipedia Organization"}"`,
        merchantname3: ` | default:"${otherStr}"`,
        merchantNameWithTitle: 'url',
        link: ['#toc li'],
        titleOrDefault: 'h1 | default:NoTitle',
        title2OrDefault: 'h1.noexistingtitle | default:NoTitle',
        performOperationOnOtherUrl: ` | default:${someOtherUrl} | tolower`,
        performOperation2OnOtherUrl: ` | default:${someOtherUrl} | tolower | getid`,
        onlySelector: x('h1'),
        otherH2s: ['h2'],
    });
    console.log(FinalObj);
})();