const xray = require('x-ray');
const filters = require('./filters');

AdditionalFilters = {
    upper: str => str.toUpperCase(),
    blah: str => str + "   Blah...Blah..."
}
for (filter in AdditionalFilters) {
    filters[filter] = AdditionalFilters[filter];
}

const x = xray({
    filters: filters
});

// ? Master Config Generator
module.exports = function (url, str2, param2) {
    masterConfig = {};

    masterConfig.meta = {
        mainUrl: '...ThisIsMymainUrl...',
        [param2 + '123']: 'not just url',
        otherStr: "Not Just WikiPedia",
        someOtherUrl: 'ThisIsSomeThingInUrl/?p=215&id=124578',
        sourceUrl: url,
        someOtherString: str2
    }
    masterConfig.data = {
        title: 'h1',
        mainUrl: ` | default:${masterConfig.meta.mainUrl}`,
        sourcUrl: ` | default:${masterConfig.meta.sourceUrl} `,
        merchantname: ' | default:"Wikipedia Organization"',
        merchantname2: ` | default:"${"Wikipedia Organization"}"`,
        merchantname3: ` | default:"${masterConfig.meta.otherStr}"`,
        merchantNameWithTitle: 'url',
        link: ['#toc li'],
        titleOrDefault: 'h1 | default:NoTitle',
        title2OrDefault: 'h1.noexistingtitle | default:NoTitle',
        performOperationOnOtherUrl: ` | default:${masterConfig.meta.someOtherUrl} | tolower`,
        performOperation2OnOtherUrlToGetId: ` | default:${masterConfig.meta.someOtherUrl} | tolower | getid`,
        capH1WithBlah: 'h1 | upper | blah',
        capH1WithSomeData: x('h1 | upper | addSomething:123456'),
        capH1: x('h1 | upper'),
        capH2: ['h2 | upper | addSomething:" **kk** " | upper'],
    }
    masterConfig.postProcess = {};

    return masterConfig;
}

// module.exports = masterConfig;