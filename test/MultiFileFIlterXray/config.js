module.exports = function (url, otherStr, someOtherUrl) {
    return {
        title: 'h1',
        url: ` | default:${url}`,
        merchantname: ' | default:"Wikipedia Organization"',
        merchantname2: ` | default:"${"Wikipedia Organization"}"`,
        merchantname3: ` | default:"${otherStr}"`,
        merchantNameWithTitle: 'url',
        link: ['#toc li'],
        titleOrDefault: 'h1 | default:NoTitle',
        title2OrDefault: 'h1.noexistingtitle | default:NoTitle',
        performOperationOnOtherUrl: ` | default:${someOtherUrl} | tolower`,
        performOperation2OnOtherUrl: ` | default:${someOtherUrl} | tolower | getid`,
    };
}


// {
//     title: 'h1',
//     // url: ` | default:${url}`,
//     merchantname: ' | default:"Wikipedia Organization"',
//     merchantname2: ` | default:"${"Wikipedia Organization"}"`,
//     // merchantname3: ` | default:"${otherStr}"`,
//     merchantNameWithTitle: 'url',
//     link: ['#toc li'],
//     titleOrDefault: 'h1 | default:NoTitle',
//     title2OrDefault: 'h1.noexistingtitle | default:NoTitle',
//     // performOperationOnOtherUrl: ` | default:${someOtherUrl} | tolower`,
//     // performOperation2OnOtherUrl: ` | default:${someOtherUrl} | tolower | getid`,
// }