const xray = require('x-ray');
const filters = require('./filters');

AdditionalFilters = {
    upper: str => str.toUpperCase(),
    blah: str => str + "   Blah...Blah..."
}
for (filter in AdditionalFilters) {
    filters[filter] = AdditionalFilters[filter];
}

const x = xray({
    filters: filters
});

// ? Master Config Generator

class MasterConfig{
    mainUrl = '...ThisIsMymainUrl...';
    someOtherUrl = 'ThisIsSomeThingInUrl/?p=215&id=124578';
    otherStr = "Not Just WikiPedia";
    
    constructor(url, str2, param2) {
        this.url = url;
        this.str2 = str2;
        this.param2 = param2;
    }
    [param2 + '123'] = 'not just url';

    data = {
        title: 'h1',
        mainUrl: ` | default:${this.mainUrl}`,
        sourcUrl: ` | default:${this.sourceUrl} `,
        merchantname: ' | default:"Wikipedia Organization"',
        merchantname2: ` | default:"${"Wikipedia Organization"}"`,
        merchantname3: ` | default:"${this.otherStr}"`,
        merchantNameWithTitle: 'url',
        link: ['#toc li'],
        titleOrDefault: 'h1 | default:NoTitle',
        title2OrDefault: 'h1.noexistingtitle | default:NoTitle',
        performOperationOnOtherUrl: ` | default:${this.someOtherUrl} | tolower`,
        performOperation2OnOtherUrlToGetId: ` | default:${this.someOtherUrl} | tolower | getid`,
        capH1WithBlah: 'h1 | upper | blah',
        capH1WithSomeData: x('h1 | upper | addSomething:123456'),
        capH1: x('h1 | upper'),
        capH2: ['h2 | upper | addSomething:" **kk** " | upper'],
    };
}

module.exports = MasterConfig;

// const merchantObj = new MasterConfig('https://en.wikipedia.org/wiki/Gabe_Newell', "This is some o ther string that is going tto be stored in config", 'XXX');
// console.log(merchantObj);
// console.log(merchantObj.data);

