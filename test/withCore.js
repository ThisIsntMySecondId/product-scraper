// ? here all the configuration including and validation takes place after which the data is passed to core.js
const retrieveAndStore = require('../core/core').retrieveAndStore;

// ? Main function
(async () => {
    // ? testing core.js
    // const merchantConfig = require('../merchant configs/Product Info/sharafdgProductHybrid');
    // const merchantConfig = require('../merchant configs/Product Info/sharafdgProductXray');
    // const merchantModel = require('./merchant models/Product Info/sharafdgProduct');
    // const merchantModel = require('./merchant models/Product Info/sharafdgProductMongooseModel');
    // const merchantModel = require('../merchant models/Product Info/sharafdgProductMongooseModel');
    // console.log(merchantModel);
    // const url = 'https://uae.sharafdg.com/product/oppo-reno-z-128gb-jet-black-cph1979-4g-dual-sim-smartphone/';
    // await requireAndStore(url, merchantConfig, merchantModel);

    // ? testing list or urls to be retrieved
    // ? it uses merchantConfig and merchantModel from above
    // ? 💡 this stuff required me to shift mongoose model making in the merchant config
    // let promiseArray = [];
    // promiseArray.push(requireAndStore('https://uae.sharafdg.com/product/oppo-f11-128gb-marble-green-4g-dual-sim-smartphone-cph1911/', merchantConfig, merchantModel));
    // promiseArray.push(requireAndStore('https://uae.sharafdg.com/product/samsung-galaxy-a70-128gb-white-sma705f-4g-lte-dual-sim-smartphone/', merchantConfig, merchantModel));
    // promiseArray.push(requireAndStore('https://uae.sharafdg.com/product/huawei-p30-lite-128gb-peacock-blue-mar-lx1m-4g-dual-sim-smartphone/', merchantConfig, merchantModel));
    // let resolvedPromise = await Promise.all(promiseArray);
    // console.log(await resolvedPromise);

    // ? testing big list or urls to be scrapped and insert into database
    // ? it uses merchantConfig and merchantModel from above
    // ? 💡 puppeteer is extreamely slow in this while xray is blazingly fast here
    // urls = [
    //     'https://uae.sharafdg.com/product/oppo-reno-z-128gb-jet-black-cph1979-4g-dual-sim-smartphone/',
    //     'https://uae.sharafdg.com/product/oppo-f11-128gb-marble-green-4g-dual-sim-smartphone-cph1911/',
    //     'https://uae.sharafdg.com/product/samsung-galaxy-a70-128gb-white-sma705f-4g-lte-dual-sim-smartphone/',
    //     'https://uae.sharafdg.com/product/huawei-p30-lite-128gb-peacock-blue-mar-lx1m-4g-dual-sim-smartphone/',
    //     'https://uae.sharafdg.com/product/samsung-galaxy-a50-128gb-black-sma505f-4g-dual-sim-smartphone/',
    //     'https://uae.sharafdg.com/product/huawei-p30-lite-128gb-midnight-black-mar-lx1m-4g-dual-sim-smartphone/',
    //     'https://uae.sharafdg.com/product/huawei-y9-prime-2019-128gb-emerald-green-4g-lte-dual-sim-smartphone/',
    //     'https://uae.sharafdg.com/product/huawei-nova-4-128gb-crush-blue-dual-sim-smartphone-vce-l22/',
    //     'https://uae.sharafdg.com/product/huawei-mate-20-pro-128gb-twilight-4g-dual-sim-smartphone-2/',
    //     'https://uae.sharafdg.com/product/huawei-p30-pro-256gb-black-4g-dual-sim-smartphone-vog-l29/',
    //     'https://uae.sharafdg.com/product/samsung-galaxy-a50-128gb-white-sma505f-4g-dual-sim-smartphone/',
    //     'https://uae.sharafdg.com/product/honor-10i-128gb-black-hrylx1t-4g-dual-sim-smartphone/',
    //     'https://uae.sharafdg.com/product/oppo-f11-128gb-fluorite-purple-4g-dual-sim-smartphone-cph1911/',
    //     'https://uae.sharafdg.com/product/huawei-p30-lite-128gb-pearl-white-mar-lx1m-4g-dual-sim-smartphone/',
    // ];

    // let promiseArray = [];

    // for (url of urls) {
    //     promiseArray.push(requireAndStore(url, merchantConfig, merchantModel));
    // }

    // let resolvedPromises = await Promise.all(promiseArray);
    // console.log(resolvedPromises);


    // ? testing list of products inserted with pupp scrapper
    // const merchantConfig = require('../merchant configs/Products List/letatengoList');
    // const merchantModel = require('../merchant models/List/letstangoListMongooseModel');

    // const url = 'https://www.letstango.com/?q=laptops&idx=letsTango_default_products&p=0&is_v=1';
    // await retrieveAndStore(url, merchantConfig, merchantModel);

    // ? testing after generating templates for configs and models
    // ? Microless List
    // const merchantConfig = require('../merchant configs/Products List/microlessList');
    // const merchantModel = require('../merchant models/List/microlessList');
    // const url = 'https://uae.microless.com/notebooks/';
    // await retrieveAndStore(url, merchantConfig, merchantModel);

    // const url = 'https://uae.microless.com/notebooks/lenovo/b/l/'
    // const xl = require('../core/scrappers/xrayProductsList').xrayRetrieveProductsList;
    // const hl = require('../core/scrappers/hybridProductsList').hybridRetrieveProductsList;
    // const pl = require('../core/scrappers/puppProductsList').puppRetrieveProductsList;
    // const fs = require('fs');
    // let data = await xl(url, merchantConfig);
    // fs.createWriteStream('./xlData.json').write(JSON.stringify(data));
    // data = await pl(url, merchantConfig);
    // fs.createWriteStream('./plData.json').write(JSON.stringify(data));
    // data = await hl(url, merchantConfig);
    // fs.createWriteStream('./hlData.json').write(JSON.stringify(data));
    // // console.log(data);

    // const url = 'https://www.ubuy.ae/en/electronics/laptops/id-565108/category-list-view/?brand=Acer%7CHP';
    // const config = require('../merchant configs/Products List/ubayList');
    // const data = await pl(url, config);
    // console.log(data);

    // ? testing after new pupp pro list with next btn click next page
    // const config = require('../merchant configs/Products List/letstengoList');
    // const url = 'https://www.letstango.com/?q=laptops&idx=letsTango_default_products&p=0&fR%5Bmanufacturer%5D%5B0%5D=Lenovo&is_v=1';
    // const data = await pl(url, config);
    // console.log(data.length);

    // ? testing everything in core after making nxt btn xlick go to nxt pg
    // ? ubuys
    // const url = 'https://www.ubuy.ae/en/electronics/laptops/id-565108/category-list-view/?brand=Acer%7CHP';
    // const config = require('../merchant configs/Products List/ubayList');
    // const ubuyModel = require('../merchant models/List/ubuyList')
    // await retrieveAndStore(url, config, ubuyModel);

    // ? letstango
    // const url = 'https://www.letstango.com/?q=laptops&idx=letsTango_default_products&p=0&fR%5Bmanufacturer%5D%5B0%5D=Lenovo&is_v=1';
    // const config = require('../merchant configs/Products List/letstengoList');
    // const ubuyModel = require('../merchant models/List/letstangoListMongooseModel')
    // await retrieveAndStore(url, config, ubuyModel);

    // ? microless
    // const url = 'https://uae.microless.com/notebooks/asus/b/l/?page=1';
    // const config = require('../merchant configs/Products List/microlessList');
    // const ubuyModel = require('../merchant models/List/microlessList')
    // await retrieveAndStore(url, config, ubuyModel);

    // ? inserting all three into single db
    // FIXME: cannot use all these three fiiles at once when modelname is same in all three models 
    // ? ubuys
    // let url = 'https://www.ubuy.ae/en/electronics/laptops/id-565108/category-list-view/?brand=Acer%7CHP';
    // let config = require('../merchant configs/Products List/ubayList');
    // let model = require('../merchant models/List/ubuyList')
    // await retrieveAndStore(url, config, model);

    // ? letstango
    // url = 'https://www.letstango.com/?q=laptops&idx=letsTango_default_products&p=0&fR%5Bmanufacturer%5D%5B0%5D=Lenovo&is_v=1';
    // config = require('../merchant configs/Products List/letstengoList');
    // model = require('../merchant models/List/letstangoListMongooseModel')
    // await retrieveAndStore(url, config, model);

    // ? microless
    // url = 'https://uae.microless.com/notebooks/asus/b/l/?page=1';
    // config = require('../merchant configs/Products List/microlessList');
    // model = require('../merchant models/List/microlessList')
    // await retrieveAndStore(url, config, model);

    // FIXME: to ask how we wuld get urls 
    // FIXME: to ask wether we should have single table or collection for list and individual product info 

    // ? scroll test
    // const config = require('../merchant configs/Products List/sharafdgList');
    // // const url = 'https://uae.sharafdg.com/iphone-11/';
    // // const url = 'https://uae.sharafdg.com/?q=iphone%2010&post_type=product';
    // const url = 'https://uae.sharafdg.com/?q=samsung&post_type=product';
    // const data = await retrieveAndStore(url, config);
    // console.log(data);
})();