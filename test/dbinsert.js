const xrayRetrieveProductInfo = require('../core/scrappers/xrayProduct').xrayRetrieveProductInfo;
const xrayRetrieveProductsList = require('../core/scrappers/xrayProductsList').xrayRetrieveProductsList;
const puppRetrieveProductInfo = require('../core/scrappers/puppProduct').puppRetrieveProductInfo;
const puppRetrieveProductsList = require('../core/scrappers/puppProductsList').puppRetrieveProductsList;
const hybridRetrieveProductInfo = require('../core/scrappers/hybridProduct').hybridRetrieveProductInfo;
const hybridRetrieveProductsList = require('../core/scrappers/hybridProductsList').hybridRetrieveProductsList;

const storeToDb = require('../core/store/db').storeToDb;

// ? Main function
(async () => {
    // 🔔 product list 🔔
    // ? inserting product list scrapped from xray scrapper
    // const config = require('../merchant configs/Products List/microlessWithInnerDetails');
    // const microlessListModel = require('../merchant models/microlessList');
    // let url = config.url;
    // let data = await xrayRetrieveProductsList(url, config);
    // console.log(data);
    // console.log(microlessListModel);
    // await storeToDb(data, microlessListModel);
    
    // 🔔 product info 🔔
    // ? inserting product info from myntra
    // const config = require('../merchant configs/Product Info/myntraProduct');
    // const myntraProductModel = require('../merchant models/myntraProduct');
    // let url = config.url;
    // let data = await hybridRetrieveProductInfo(url, config);
    // console.log(data);
    // console.log("retrieval done")
    // await storeToDb(data, myntraProductModel);
    
    // 🔔 product info 🔔
    // ? insert product info from letstango with a different schema to see if there can exists two documents in db with different schemas
    // const config = require('../merchant configs/Product Info/LetstangoProductPupp');
    // const letstangoProductModelSmall = require('../merchant models/letstangoProductSmall');
    // let url = config.url;
    // let data = await xrayRetrieveProductInfo(url, config);
    // console.log(data);
    // console.log("retrieval done");
    // await storeToDb(data, letstangoProductModelSmall);
    
    // 🔔 product info 🔔
    // ? retrieving data, updating data after retrieval and then inserting to see if the document update or not
    // const config = require('../merchant configs/Product Info/LetstangoProductPupp');
    // const letstangoProductModelSmall = require('../merchant models/letstangoProductSmall');
    // let url = config.url;
    // let data = await xrayRetrieveProductInfo(url, config);
    // console.log(data);
    // data.mrp = 'AED 50,000.00';
    // console.log("retrieval done");
    // await storeToDb(data, letstangoProductModelSmall);

    // 🔔 product info 🔔
    // ? retrieving product info with large data and inserting it into db with a very complex schema
    // ? this is the required db
    const config = require('../merchant configs/Product Info/sharafdgProductHybrid'); //? for all three urls in config files
    // const config = require('../merchant configs/Product Info/letstangoProductHybrid');
    let url = config.url;
    let data = await xrayRetrieveProductInfo(url, config);
    console.log(data);
    console.log("retrieval done");
    const sharafdgProductModel = require('../merchant models/Product Info/sharafdgProduct');
    // const sharafdgProductModel = require('../merchant models/Product Info/letstangoProduct');
    await storeToDb(data, sharafdgProductModel);
})();