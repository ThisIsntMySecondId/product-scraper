const puppeteer = require('puppeteer');

// ? Main
(async () => {
    const browser = await puppeteer.launch({ headless: false, defaultViewport: null });
    const page = await browser.newPage();
    const url = 'https://uae.microless.com/notebooks/hp/b/l/?page=1';
    // ? do 👇 not working with this
    // const url = 'https://www.letstango.com/?q=laptops&idx=letsTango_default_products&p=0&fR%5Bmanufacturer%5D%5B0%5D=Lenovo&is_v=1';
    await page.goto(url, { timeout: 0 });
    // ? Config Details
    const config = {
        scope: '.hits .dealblock_thump',
        mainData: {
            title: 'h1',
            price: 'h2',
            link: '.thump_img a',
            image: '.thump_img a img',
        }
    }
    // ? Loop Initialization vars
    let nextBtn;
    const limit = 5;
    let pageNo = 1;
    while (pageNo <= limit) {
        await page.waitFor(3000)
        let proConts = await page.$$(config.scope);
        for (proCont of proConts) {
            let heading = await proCont.$eval(config.mainData.title, (e) => { return e.innerText; });
            console.log(heading);
        }
        nextBtn = await page.$('.ais-pagination li.ais-pagination--item__active + li a');
        await nextBtn.click();
        pageNo++;
    }
    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    await browser.close();
})();