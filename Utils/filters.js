const filters = {
    trim: str => str.trim(),
    upper: str => str.toUpperCase(),
    capatilize: x => x.toUpperCase(),
    toFloat: x => parseFloat(x.replace('AED ', '').replace(',', '')),
    default: (x, str) => {
        if (x.length === 0) return str;
    },
}

module.exports = filters;