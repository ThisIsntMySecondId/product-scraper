const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const parse = require('x-ray-parse');

// ? temp write to file
const fs = require('fs');
const file = fs.createWriteStream('./letstangoFinal.json');

// const config = require('./tempfinalConfig.js');
const config = require('./myntraList.js');
// const config = require('./myntraProduct.js');
// const config = require('./sharafdgProduct.js');
// const config = require('./microless.js');
// const config = require('./sprii.js');

// console.log(config);
// ?🤔 we can do this stuff in the c onfig file itself
for (key in config.mainData) {
    config['mainData'][key] = parse(config.mainData[key]);
}
for (key in config.innerDetails) {
    config['innerDetails'][key] = parse(config.innerDetails[key]);
}
config.innerDetailSource = config.innerDetailSource ? parse(config.innerDetailSource) : null;
config.nextPage = config.nextPage ? parse(config.nextPage) : null;
console.log(config);

/*
    todo: find unique selector or verbs for : 
        1️⃣ this can also be done
        2️⃣ not required
        3️⃣ i found this
        4️⃣ 
*/

// todo: to make it working with filters ✅
// todo: to make it working for multiple pages fetching ✅
// todo: to make it working with or without scope variable ✅
// todo: to make it working for relative as well as absolute urls ✅
// todo: to make it working for url or array of urls (good but not that necessary)
// todo: to make config file available form console (good but not that necessary)

(async function () {
    const browser = await puppeteer.launch({ headless: false, defaultViewport: null });
    const page = await browser.newPage();
    let finalResult = [];

    if (!config.scope) {
        let url = config.url;
        await page.goto(url);

        // ? fetching main details
        let mainDetails = {};
        // todo: to rename main data to main details
        for (let key in config.mainData) {
            let { selector: sel, attribute: attr, filters: fils } = config.mainData[key];
            mainDetails[key] = attr ? await page.$eval(sel, (elem, attr) => elem.getAttribute(attr), attr) : await page.$eval(sel, (elem) => elem.innerText);
            if (fils.length > 0) {
                for (fil of fils) {
                    if (fil.name in config.filters) {
                        mainDetails[key] = config.filters[fil.name](mainDetails[key]);
                    }
                }
            }
        }
        finalResult = mainDetails;

    } else {
        // ? Method 1️⃣ one get individual product containers and then run the function to get required data in each element
        // todo: make it work ✅
        // todo: make filters work ✅
        // todo: make pagination work ✅
        // todo: make innerDetails work
        let url = config.url;
        let pageNo = 1;
        const limit = config.limit || 1;
        // const limit = 1;
        do {
            await page.goto(url);
            let proConts = await page.$$(config.scope);
            let singlePageResults = []; //? variable to store results retrieved from single page
            for (proCont of proConts) {
                // console.log("start");

                // ? fetching main details
                let mainDetails = {};
                // todo: to rename main data to main details
                for (let key in config.mainData) {
                    let { selector: sel, attribute: attr, filters: fils } = config.mainData[key];
                    //// console.log("==>", sel, attr);
                    //// console.log(await proCont.$eval(sel, e => e.innerText) );
                    ////console.log("==> innerttext ", await proCont.$eval(sel, (elem) => elem.innerText));
                    ////console.log("==>", attr ? await proCont.$eval(sel, (elem, attr) => elem.getAttribute(attr), attr) : await proCont.$eval(sel, (elem) => elem.innerText));
                    mainDetails[key] = attr ? await proCont.$eval(sel, (elem, attr) => elem.getAttribute(attr), attr) : await proCont.$eval(sel, (elem) => elem.innerText);
                    if (fils.length > 0) {
                        for (fil of fils) {
                            if (fil.name in config.filters) {
                                mainDetails[key] = config.filters[fil.name](mainDetails[key]);
                            }
                        }
                    }
                }
                // console.log("after mainDetails");

                // ? fetching inner details
                // todo: make filters work for inner details
                // if (config.innerDetails) {
                //     individualUrl = await proCont.$eval(config.innerDetailSource.selector, e => e.href);
                //     await page.goto(individualUrl);

                //     //? fetch inner details
                //     let innerDetails = {};
                //     // for every required inner detail
                //     for (let key in config.innerDetails) {
                //         let { selector: sel, attribute: attr } = config.innerDetails[key];
                //         innerDetails[key] = attr
                //             ? await page.$eval(sel, (elem, attr) => elem.getAttribute(attr), attr)
                //             : await page.$eval(sel, (elem) => elem.innerText);
                //     }
                //     mainDetails["details"] = innerDetails; //? storing fetched inner details in data
                //     console.log(mainDetails["details"]);
                //     await page.goto(url); //? redirecting back to product listing page
                // }

                singlePageResults.push(mainDetails); //? pushing everything
            }
            finalResult.push(...singlePageResults);

            if (!!config.nextPage) {
                url = await page.$eval(config.nextPage.selector, e => e.getAttribute('href'));
                if (config.merchant && !url.includes(config.merchant)) {
                    console.log(true);
                    url = config.merchant + url;
                }
            }
            console.log(url);
            console.log(pageNo);
            console.log(limit);
            pageNo++;
        } while ((pageNo <= limit) && url)
    }


    // ? Method 2️⃣ two run method to get each element directly in the page itself. Get individual product containers and find required info in them
    // let url = config.url;
    // let pageNo = 0;
    // const limit = config.limit || 1;
    // let finalResult = [];
    // do {
    //     await page.goto(url);
    //     const singlePageResult = await page.evaluate((config) => {
    //         let details = [];
    //         console.log(config);
    //         //window.localStorage["config"] = config;
    //         // for every product container
    //         for (let proCont of document.querySelectorAll(config.scope)) {
    //             let data = {};
    //             // for every required field
    //             for (let key in config.mainData) {
    //                 let { selector: sel, attribute: attr } = config.mainData[key];
    //                 data[key] = attr ? proCont.querySelector(sel).getAttribute(attr) : proCont.querySelector(sel).innerText;
    //             }

    //             // ? need to call details page here
    //             // todo: code to fetch details section and add to data[details] section
    //             // todo: change the variable name (details) which stores  innerDetails
    //             if (config.innerDetails) {
    //                 individualUrl = proCont.querySelector(config.innerDetailSource.selector).href;
    //                 window.location.href = individualUrl;
    //                 console.log("window.localStorage.config");

    //                 //? fetch details
    //                 let details = {};
    //                 // for every required detail
    //                 for (let key in config.innerDetails) {
    //                     let { selector: sel, attribute: attr } = config.innerDetails[key];
    //                     details[key] = attr ? proCont.querySelector(sel).getAttribute(attr) : proCont.querySelector(sel).innerText;
    //                 }
    //                 data["details"] = details //? storing fetched inner details in data
    //                 window.location.href = url //? redirecting back to product listing page
    //             }

    //             // ? finally pushing add data from single page to its own array
    //             // console.log(data);
    //             details.push(data);
    //         }
    //         return details;
    //     }, config);

    //     // ? filters
    //     // todo: remember to not to do this. Instead of filter data the moment you retrieve from webpage
    //     for (data of singlePageResult) {
    //         for (key in config.mainData) {
    //             let { selector: sel, attribute: attr, filters: fils } = config.mainData[key];
    //             if (fils.length > 0) {
    //                 for (fil of fils) {
    //                     if (fil.name in config.filters) {
    //                         data[key] = config.filters[fil.name](data[key]);
    //                     }
    //                 }
    //             }

    //         }
    //     }
    //     finalResult.push(singlePageResult);
    //     if (!!config.nextPage) {
    //         url = await page.$eval(config.nextPage.selector, e => e.getAttribute('href'));
    //     }
    //     // console.log(singlePageResult);
    //     pageNo++;
    // } while ((pageNo < limit) && url)

    // ? Final result output after filtering ✅, pagination ✅ and details fetching
    console.log(finalResult);
    console.log(finalResult.length);

    file.write(JSON.stringify(finalResult));

    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    browser.close();

})().catch(err => {
    if (err.message === 'Navigation failed because browser has disconnected!') {
        console.log('\x1b[43m\x1b[30m%s\x1b[0m', ' You have closed the browser or some tab');
    }
});