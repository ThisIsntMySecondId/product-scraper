const Xray = require('x-ray');
const htmltotext = require('html-to-text');
const fs = require('fs');
const mongoose = require('mongoose');
const crypto = require('crypto');
const DataTransform = require('node-json-transform').DataTransform;


const x = Xray({
    filters: {
        trim: str => str.trim(),
    }
});

// ? 🔨🔪 Utility Functions
const sha256 = x => crypto.createHash('sha256').update(x, 'utf8').digest('hex');
Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
}
const isUrl = (url) => {
    var expression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
    var regex = new RegExp(expression);

    if (url.match(regex)) {
        return true;
    } else {
        return false;
    }
}

const retriveData = async (config) => {
    // ? declaring empty array to store all retrieved data
    let data = [];

    console.log(config);

    // ? If url in config is not an array convert it to array
    let urls = Array.isArray(config.url) ? config.url : [].concat(config.url);

    // ? If individual details about the product have to be scrapped
    if (config.innerDetails) {
        config.mainData['details'] = x(config.innerDetailsSource, config.innerDetails);
    }

    // ? If no page limit is mentioned set it to 1 by default
    if (!config.pageLimit) {
        config.pageLimit = 1;
    }

    if (!config.scope) {
        // ? scrape data from the array of urls
        for (url of urls) {
            // ? For each individual url get the data from it and store it in some temporary variable
            let dataFromOneurl = await x(url, config.mainData);

            // ? push the data retrieved from above to the main data array
            data.push(dataFromOneurl);
        }
    } else {
        // ? scrape data from the array of urls
        for (url of urls) {
            // ? For each individual url get the data from it and store it in some temporary variable
            let dataFromOneurl = await x(url, config.scope, [config.mainData]).paginate(config.nextPage).limit(config.pageLimit);

            // ? push the data retrieved from above to the main data array
            data.push(dataFromOneurl);
        }
    }

    // ? to flatten the data array containing array of data of individual urls
    data = data.flat();

    // ? Return the final data
    return data;

}

// ? Store data directly to json file
// @param 1️⃣ the data array to be stored which is returned from retriveData function
// @param 2️⃣ file name
const storeToFileJSON = async (data, filename) => {

    // todo: remember to make filename available directly from  console
    try {
        const file = fs.createWriteStream(filename);
        file.write(JSON.stringify(data));
        file.end();
    } catch (err) {
        console.log(err)
    }
}

// ? Store data directly to csv file
// @param 1️⃣ the data array to be stored which is returned from retriveData function
// @param 2️⃣ file name
const storeToFileCSV = async (data, filename) => {

    // todo: remember to make filename available directly from  console
    try {
        const file = fs.createWriteStream(filename);
        // ? Writing csv file header with the attribute names of the first data element
        const keys = Object.keys(data[0]);
        file.write(keys.join(',') + '\n');

        // ? Writing individual records
        for (record of data) {
            let str = ''
            for (key of keys) {
                str += `\"${record[key]}\", `;
            }
            str += '\n';
            file.write(str);
        }
        file.end();
    } catch (err) {
        console.log(err)
    }
}

// ? Store data directly to mongodb database
// @param the data array to be stored which is returned from retriveData function
const storeToDb = async (data, dbConfig) => {
    console.log(dbConfig);
    const dburl = dbConfig.connectionUrl;
    try {
        await mongoose.connect(dburl, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('\x1b[44m\x1b[37m%s\x1b[0m', " Yey!!! we are connected ");
        const schema = new mongoose.Schema(dbConfig.schema, { timestamps: true });
        const dataModel = mongoose.model(dbConfig.modelname, schema);

        // todo: fix the nexted object insertition into db
        let dataTransform = DataTransform(data, dbConfig.dataMapping);
        let refinedData = dataTransform.transform(); //? the flattened data objet with node-json-transform by using mappings from dbconfig

        for (obj of refinedData) {
            let res = await dataModel.updateMany({ _id: sha256(obj[dbConfig.fieldForId]) }, obj, { upsert: true });
            console.log(res);
        }

        mongoose.disconnect();
    } catch (err) {
        console.log(err)
    }
}


// ? Main function
(async () => {
    let merchantConfig, mode, file;

    // ? get the merchant config file from console
    if (!process.argv[2]) {
        console.log('\x1b[41m\x1b[37m %s \x1b[0m', 'Config File Not Found Error: Please provide a configuration file');
        process.exit();
    }
    try {
        merchantConfig = require(process.argv[2]);
    } catch (err) {
        if (err.code == 'MODULE_NOT_FOUND') {
            console.log('\x1b[41m\x1b[37m %s \x1b[0m', 'Config File Not Found Error: Please Make sure that you are entering correct confige file name');
            process.exit();
        }
    }

    // ? get the store mode file from console
    if (!process.argv[3]) {
        console.log('\x1b[43m\x1b[30m %s \x1b[0m', 'Storage Mode is not provided. The output will be displayed to the console');
        mode = 'console';
    } else {
        if (!process.argv[3].includes('=') || process.argv[3].split('=')[0] != 'store' || !["json", "csv", "db"].includes(process.argv[3].split('=')[1])) {
            console.log('\x1b[41m\x1b[37m %s \x1b[0m', 'Invalid Storage Mode Error: Please provide a proper storage mode');
            console.log('\x1b[44m\x1b[37m %s \x1b[0m', 'Storage Mode Syntax : store=<storage_mode> <option>');
            console.log('storage_mode can be : ');
            console.log('json and option for that will be json filename where the data to be stored');
            console.log('csv and option for that will be csv filename where the data to be stored');
            console.log('db and option for that will be dbconfig file containing db details');
            process.exit();
        } else {
            mode = process.argv[3].split('=')[1]
        }
    }

    // ? get where to store data filename or dbconfig file
    if (process.argv[3] && !process.argv[4]) {
        console.log('\x1b[41m\x1b[37m %s \x1b[0m', 'File Not Found Error. Please provide a file name if you want to store data in json or csv file or the config file if you want to store data in db');
    } else {
        file = process.argv[4];
    }
    console.log(mode);
    console.log(file);

    let dataFromWebsite = await retriveData(merchantConfig);
    console.log("Data Retrieved");

    switch (mode) {
        case 'json': storeToFileJSON(dataFromWebsite, file); break;
        case 'csv': storeToFileCSV(dataFromWebsite, file); break;
        case 'db':
            try {
                const dbConfig = require(file);
                storeToDb(dataFromWebsite, dbConfig);
            } catch (err) {
                // Todo: remember to make the specific message is file not found
                console.log(err);
            }
            break;
        case 'console': console.log(dataFromWebsite); break;
        case 'default': console.log('please provide proper inputs'); process.exit();
    }

})();