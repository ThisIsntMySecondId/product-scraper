const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const parse = require('x-ray-parse');
const xray = require('x-ray');
const htmlToText = require('html-to-text');
const fs = require('fs');
const file = fs.createWriteStream('./letstangoHybridFinal.json');

const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// const config = require('./tempfinalConfig.js');
// const config = require('./myntraConfig.js');
// const config = require('./myntraConfig2.js');
const config = require('./sharafdgProduct.js');
// const config = require('./letstangoProduct.js');

(async function () {
    const browser = await puppeteer.launch({ headless: false, defaultViewport: null });
    const page = await browser.newPage();
    let finalResult = [];

    if (!config.scope) {
        await page.goto(config.url);

        const pageHTML = await page.evaluate(() => { return document.querySelector('html').innerHTML; })
        console.log("pageHTML");
        finalResult = await x(pageHTML, config.mainData);
    }

    console.log(finalResult);
    finalResult.description = htmlToText.fromString(finalResult.description);
    file.write(JSON.stringify(finalResult));

    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    browser.close();

})().catch(err => {
    if (err.message === 'Navigation failed because browser has disconnected!') {
        console.log('\x1b[43m\x1b[30m%s\x1b[0m', ' You have closed the browser or some tab');
    }
});