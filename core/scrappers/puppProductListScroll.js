const puppeteer = require('puppeteer');
const parse = require('x-ray-parse');
const filters = require('../../Utils/filters');

// ? scrollDelay is the time between two consecutive scrolls as well as the delay before the first scroll
const puppScrollRetrieveProductsList = async (url, config, scrollDelay = 2000) => {

    // ? parse the config
    for (key in config.mainData) {
        config['mainData'][key] = parse(config.mainData[key]);
    }
    if (!config.scope) return false;

    // ? initialize puppeteer
    const browser = await puppeteer.launch({ headless: false, defaultViewport: null });
    const page = await browser.newPage();
    await page.goto(url);
    await page.waitFor(scrollDelay);

    // ? start scrolling till page limit
    // TODO: limit either by item count or something else
    // TODO: when user needs all the items
    // FIXME: goes into infinite loop when more item are required then the total no that exists actually on page 
    // eg items on page are 195 and when user asks 200 items
    if (config.limit >= 0) {
        let requiredCount = config.limit || (await page.$$(config.scope)).length;
        while ((await page.$$(config.scope)).length < requiredCount) {
            let previousHeight = await page.evaluate('document.body.scrollHeight');
            await page.evaluate("window.scrollBy({top: document.body.scrollHeight, left: 0, behavior: 'smooth'})");
            await page.waitForFunction(`document.body.scrollHeight > ${previousHeight}`);
            await page.waitFor(scrollDelay);
        }
    } else {
        while (await page.evaluate('(window.innerHeight + document.scrollingElement.scrollTop) != document.scrollingElement.scrollHeight')) {
            await page.evaluate("window.scrollBy({top: document.body.scrollHeight, left: 0, behavior: 'smooth'})");
            await page.waitFor(scrollDelay);
        }
    }

    console.log("scrolled");
    // ? scrape entire data on page and store result
    // ? proConts => product containers
    const proConts = await page.$$(config.scope);
    console.log('Final Item Count = ', proConts.length);

    let results = [];
    for (proCont of proConts) {
        let mainDetails = {};
        // TODO: rename mainData to mainDetails
        // TODO: return null if element doesnt exists
        for (let key in config.mainData) {
            let { selector: sel, attribute: attr, filters: fils } = config.mainData[key];
            if (await proCont.$(sel)) {
                mainDetails[key] = attr ? await proCont.$eval(sel, (elem, attr) => elem.getAttribute(attr), attr) : await proCont.$eval(sel, (elem) => elem.innerText);
            } else {
                mainDetails[key] = '';
            }
            if (fils.length > 0) {
                for (fil of fils) {
                    if (fil.name in filters) {
                        mainDetails[key] = filters[fil.name](mainDetails[key]);
                    }
                }
            }
        }
        results.push(mainDetails);
    }

    // ? close and return
    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    await browser.close();
    // FIXME: when user requires more items then the items are on the page 
    if(config.limit >= 0){
        let itemsToBeRemoved = results.length - config.limit;
        results.splice(results.length - itemsToBeRemoved, itemsToBeRemoved);
    }
    return results;
}

module.exports = { puppScrollRetrieveProductsList };