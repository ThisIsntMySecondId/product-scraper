const puppeteer = require('puppeteer');
const Xray = require('x-ray');
const filters = require('../../Utils/filters');

const x = Xray({
    filters: filters
});

// ? should give list of products details from the products page as per the provided config
// ? make sure your config file have a scope property
// @param 1️⃣ url from which data needs to be retrieved
// @param 2️⃣ config file for which the data to be retrieve
// @return retrieved data => array of results that were retrieved from product listing page
const hybridRetrieveProductsList = async (url, config) => {

    // ? init puppeteer
    const browser = await puppeteer.launch({ headless: false, defaultViewport: null });
    const page = await browser.newPage();
    let finalResult = [];

    let pageNo = 1;
    const limit = config.limit || 1;
    do {
        await page.goto(url, {timeout: 0});
        const pageHTML = await page.evaluate(() => { return document.querySelector('html').innerHTML; });
        let singlePageResults = await x(pageHTML, config.scope, [config.mainData]).paginate(config.nextPage).limit(config.limit);
        finalResult.push(...singlePageResults);

        // ! this will not work here like this
        // todo: mute this feature
        //  ? this feature now has its own module
        // ? If individual details about the product have to be scrapped
        // if (config.innerDetails) {
        //     config.mainData['details'] = x(config.innerDetailsSource, config.innerDetails);
        // }

        // TOFIX: for patya for fixig the url of next page
        if (!!config.nextPage) {
            url = await page.$eval(config.nextPage.replace('@href',''), e => e.getAttribute('href'));
            if (config.merchant && !url.includes(config.merchant)) {
                url = config.merchant + url;
            }
        }
        pageNo++;
    } while ((pageNo <= limit) && url)

    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    browser.close();

    return finalResult;
}

module.exports = { hybridRetrieveProductsList };