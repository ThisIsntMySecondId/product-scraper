const puppeteer = require('puppeteer');
const parse = require('x-ray-parse');
const filters = require('../../Utils/filters');

// ? should give list of products details from the products page as per the provided config
// ? make sure your config file have a scope property
// @param 1️⃣ url from which data needs to be retrieved
// @param 2️⃣ config file for which the data to be retrieve
// @return retrieved data => array of results that were retrieved from product listing page
const puppRetrieveProductsList = async (url, config) => {
    // ? preparing config file
    // ?🤔 we can do this stuff in the config file itself
    for (key in config.mainData) {
        config['mainData'][key] = parse(config.mainData[key]);
    }
    for (key in config.innerDetails) {
        config['innerDetails'][key] = parse(config.innerDetails[key]);
    }
    config.innerDetailSource = config.innerDetailSource ? parse(config.innerDetailSource) : null;
    config.nextPage = config.nextPage ? parse(config.nextPage) : null;
    config.nextBtn = config.nextBtn ? parse(config.nextBtn) : null;
    if (!config.scope) return false;

    // ? init puppeteer
    const browser = await puppeteer.launch({ headless: false, defaultViewport: null });
    const page = await browser.newPage();
    let finalResult = [];

    // ? Method 1️⃣ one get individual product containers and then run the function to get required data in each element
    let pageNo = 1;
    const limit = config.limit || 1;
    await page.goto(url, { timeout: 0 });
    let nextBtn;
    while (pageNo <= limit) {
        let proConts = await page.$$(config.scope);
        let singlePageResults = []; //? variable to store results retrieved from single page
        for (proCont of proConts) {
            // ? fetching main details
            let mainDetails = {};
            // TODO: rename mainData to mainDetails
            for (let key in config.mainData) {
                let { selector: sel, attribute: attr, filters: fils } = config.mainData[key];
                if (await proCont.$(sel)) {  //FIXME: if the selector is empty then select the parent element 
                    mainDetails[key] = attr ? await proCont.$eval(sel, (elem, attr) => elem.getAttribute(attr), attr) : await proCont.$eval(sel, (elem) => elem.innerText);
                } else {
                    mainDetails[key] = '';
                }
                if (fils.length > 0) {
                    for (fil of fils) {
                        if (fil.name in filters) {
                            mainDetails[key] = filters[fil.name](mainDetails[key]);
                        }
                    }
                }
            }

            // FIXME: make it able to find other elements or continue to next so that the program doesnt exits with wierd looking error
            // TODO: see if you can use async await promise magic to fast it up
            // ! remember to not to run all the promise at once as it will overflow nodes default memory size
            // ? fetching inner details
            // ! Dont use it for pages with too many products as its gonna take a hell lot of time
            if (config.innerDetails) {
                individualUrl = await proCont.$eval(config.innerDetailSource.selector, e => e.href);
                const page2 = await browser.newPage();
                await page2.goto(individualUrl, { timeout: 0 });

                //? fetch inner details
                // for every required inner detail
                for (let key in config.innerDetails) {
                    let { selector: sel, attribute: attr } = config.innerDetails[key];
                    // innerDetails[key] = attr
                    mainDetails[key] = attr
                        ? await page2.$eval(sel, (elem, attr) => elem.getAttribute(attr), attr)
                        : await page2.$eval(sel, (elem) => elem.innerText);
                }
                // mainDetails["details"] = innerDetails; //? storing fetched inner details in data
                await page2.close(); //? redirecting back to product listing page
            }
            singlePageResults.push(mainDetails); //? pushing everything
        }
        console.log(singlePageResults.length);
        finalResult.push(...singlePageResults);


        if (config.nextPage) {
            if (pageNo == limit) break;

            // await page.$eval(config.nextPage.selector, e => {
            //     e.scrollIntoView({ behavior: "smooth" });
            // });
            // await page.waitFor(5000);

            url = await page.$eval(config.nextPage.selector, e => e.getAttribute('href'));
            if (config.merchant && !url.includes(config.merchant)) {
                url = config.merchant + url;
            }
            await page.goto(url, { timeout: 0 });
            pageNo++;
        } else if (config.nextBtn) {
            if (pageNo == limit) break;

            // await page.$eval(config.nextBtn.selector, e => {
            //     e.scrollIntoView({ behavior: "smooth", block: "end" });
            // });
            // await page.waitFor(5000);

            console.log("In Button");
            nextBtn = await page.$(config.nextBtn.selector);

            // await Promise.all([
            //     nextBtn.click(),
            //     page.waitForNavigation({ waitUntil: "networkidle0" })
            // ])
            await nextBtn.click(),
            await page.waitFor(5000);      //FIXME: you dont have to hardcode this value. Because some pages can take more time then 5s to load 
            pageNo++;
        } else {
            pageNo++;
            if (config.limit > 1) {
                console.log("nextPage or nextBtn not found. Cannot move to next source");
                break;
            }
        }
    }

    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    browser.close();

    return finalResult;
}

module.exports = { puppRetrieveProductsList };
