// ? Modules
const Xray = require('x-ray');
const filters = require('../../Utils/filters');

const x = Xray({
    filters: filters
});

// ? should give list of products details from the products page as per the provided config
// ? make sure your config file have a scope property
// @param 1️⃣ url from which data needs to be retrieved
// @param 2️⃣ config file for which the data to be retrieve
// @return retrieved data => object of results that were retrieved from individual product page
const xrayRetrieveProductInfo = async (url, config) => {
    let data;

    if (config.innerDetails) {
        config.mainData['details'] = x(config.innerDetailsSource, config.innerDetails);
    }

    data = await x(url, config.mainData);

    // ? Return the final data
    return data;
}

module.exports = { xrayRetrieveProductInfo };