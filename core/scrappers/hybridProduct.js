const puppeteer = require('puppeteer');
const Xray = require('x-ray');
const filters = require('../../Utils/filters');

const x = Xray({
    filters: filters
});

// ? should give list of products details from the products page as per the provided config
// ? make sure your config file have a scope property
// @param 1️⃣ url from which data needs to be retrieved
// @param 2️⃣ config file for which the data to be retrieve
// @return retrieved data => object of results that were retrieved from individual product page
const hybridRetrieveProductInfo = async (url, config) => {
    const browser = await puppeteer.launch({ headless: false, defaultViewport: null });
    const page = await browser.newPage();

    // TODO: try to only load the html doc of page instead of every resource --refer to chrome debug tool network tab doc resources for this
    await page.goto(url, { timeout: 0 });

    const pageHTML = await page.evaluate(() => { return document.querySelector('html').innerHTML; })
    let finalResult = await x(pageHTML, config.mainData);


    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    browser.close();

    return finalResult;
}

module.exports = { hybridRetrieveProductInfo };