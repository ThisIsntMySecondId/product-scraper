// ? Modules
const Xray = require('x-ray');
const filters = require('../../Utils/filters');

const x = Xray({
    filters: filters
});
// ? should give list of products details from the products page as per the provided config
// ? make sure your config file have a scope property
// @param 1️⃣ url from which data needs to be retrieved
// @param 2️⃣ config file for which the data to be retrieve
// @return retrieved data => array of results that were retrieved from product listing page
const xrayRetrieveProductsList = async (url, config) => {
    let data = [];
    // todo: mute this feature
    //  ? this feature now has its own module
    // ? If individual details about the product have to be scrapped
    if (config.innerDetails) {
        config.mainData['details'] = x(config.innerDetailsSource, config.innerDetails);
    }

    // ? If no page limit is mentioned set it to 1 by default
    data = await x(url, config.scope, [config.mainData]).paginate(config.nextPage).limit(config.limit || 1);

    // refer todo
    if (config.innerDetails) {
        for (datum of data) {
            for (key in config.innerDetails) {
                datum[key] = datum.details[key];
            }
            delete datum['details'];
        }
    }

    return data;
}

module.exports = { xrayRetrieveProductsList }