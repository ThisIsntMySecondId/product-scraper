const puppeteer = require('puppeteer');
const parse = require('x-ray-parse');
const filters = require('../../Utils/filters');

// FIXME: fix its taking to much time
// FIXME: model config files
// FIXME: max event listener limit from 4 to more


// ? should give list of products details from the products page as per the provided config
// ? make sure your config file have a scope property
// @param 1️⃣ url from which data needs to be retrieved
// @param 2️⃣ config file for which the data to be retrieve
// @return retrieved data => object of results that were retrieved from individual product page
const puppRetrieveProductInfo = async (url, config) => {

    // ?🤔 we can do this stuff in the c onfig file itself
    // ? parsing config file
    for (key in config.mainData) {
        config['mainData'][key] = parse(config.mainData[key]);
    }

    // ? starting puppeteer
    const browser = await puppeteer.launch({ headless: false, defaultViewport: null });
    const page = await browser.newPage();
    await page.goto(url, { timeout: 0 });

    // ? fetching main details
    let mainDetails = {};
    // TODO: to rename main data to main details
    for (let key in config.mainData) {
        let { selector: sel, attribute: attr, filters: fils } = config.mainData[key];
        mainDetails[key] = attr ? await page.$eval(sel, (elem, attr) => elem.getAttribute(attr), attr) : await page.$eval(sel, (elem) => elem.innerText);
        if (fils.length > 0) {
            for (fil of fils) {
                if (fil.name in filters) {
                    mainDetails[key] = filters[fil.name](mainDetails[key]);
                }
            }
        }
    }
    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    browser.close();
    return mainDetails;
}

module.exports = { puppRetrieveProductInfo };