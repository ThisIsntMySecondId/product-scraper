// NOTE : The model used here is the mongoose model that needs to be included from the core or other fn that is calling this fn
// ? this will take mongoose model as input along with data and stores the result in the db
// TODO: for products without id use either link or title to produce id
const { sha256 } = require('../../Utils/utils');
const mongoose = require('mongoose');
const config = require('../../core/config');

// FIXME: the docmentation of this function
// ? Store data directly to mongodb database
// @param the data array to be stored which is returned from retriveData function
// @param the mongoose model
const storeToDb = async (data, model) => {
    const dburl = config.dbUrl;
    try {
        await mongoose.connect(dburl, { useNewUrlParser: true, useUnifiedTopology: true });

        if (Array.isArray(data)) {
            for (obj of data) {
                // FIXME: for products that doesnt have id provide interface to pprovide field for id. //? right now it uses hashed product link by default
                let res = await model.updateMany({ _id: obj.id || (!!obj.link && sha256(obj.link)) || sha256(obj.title) }, obj, { upsert: true });
            }
        } else {
            // FIXME: for products that doesnt have id provide interface to pprovide field for id. //? right now it uses hashed product link by default
            let res = await model.updateMany({ _id: data.id || (!!data.link && sha256(data.link)) || sha256(data.title) }, data, { upsert: true });
        }

        mongoose.disconnect();
    } catch (err) {
        console.log(err)
    }
}

module.exports = { storeToDb };