// ? It will take url, merchant config, and model from user and retrieve data and store it in db

// TODO: make simple errors appears to console instead of those unresolved blah blah
const xrayRetrieveProductInfo = require('./scrappers/xrayProduct').xrayRetrieveProductInfo;
const xrayRetrieveProductsList = require('./scrappers/xrayProductsList').xrayRetrieveProductsList;
const puppRetrieveProductInfo = require('./scrappers/puppProduct').puppRetrieveProductInfo;
const puppRetrieveProductsList = require('./scrappers/puppProductsList').puppRetrieveProductsList;
const hybridRetrieveProductInfo = require('./scrappers/hybridProduct').hybridRetrieveProductInfo;
const hybridRetrieveProductsList = require('./scrappers/hybridProductsList').hybridRetrieveProductsList;
const puppScrollRetrieveProductsList = require('./scrappers/puppProductListScroll').puppScrollRetrieveProductsList;
const storeToDb = require('./store/db').storeToDb;

const retrieveAndStore = async (url, merchantConfig, merchantModel) => {
    const scrapper = merchantConfig.mode + merchantConfig.type;
    let data;

    switch (scrapper) {
        case 'xrayinfo':
            data = await xrayRetrieveProductInfo(url, merchantConfig);
            break;
        case 'xraylist':
            data = await xrayRetrieveProductsList(url, merchantConfig);
            break;
        case 'puppinfo':
            data = await puppRetrieveProductInfo(url, merchantConfig);
            break;
        case 'pupplist':
            data = await puppRetrieveProductsList(url, merchantConfig);
            break;
        case 'hybridinfo':
            data = await hybridRetrieveProductInfo(url, merchantConfig);
            break;
        case 'hybridlist':
            data = await hybridRetrieveProductsList(url, merchantConfig);
            break;
        case 'puppscrolllist':
            data = await puppScrollRetrieveProductsList(url, merchantConfig);
            break;
        default:
            throw new Error("Scrapping Mode Not Available. please make sure that you have (scrapping) mode set up in the merchant config");
    }

    if (merchantModel) {
        await storeToDb(data, merchantModel);
    } else {
        return data;
    }
}

module.exports = { retrieveAndStore };